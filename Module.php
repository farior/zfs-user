<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 12:46
 */

namespace ZFS\User;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\EventManager\EventInterface;
use Zend\Mvc\MvcEvent;
use ZFS\User\Model\Object\Role as RoleObject;
use Zend\Console\Request as ConsoleRequest;
use Zend\Stdlib\ArrayUtils;
use ZFS\User\EventManager\Profile as ProfileEvent;
use ZFS\User\EventManager\UserManagement as UserManagementEvent;
use ZFS\User\EventManager\Rbac as RbacEvent;

class Module implements ConfigProviderInterface, DependencyIndicatorInterface, BootstrapListenerInterface
{
    const ROOT_DIR = __DIR__;
    const PROFILE_BOX_RENDER = 'ProfileBox';

    public function onBootstrap(EventInterface $e)
    {
        if ((!$e instanceof MvcEvent) || ($e->getRequest() instanceof ConsoleRequest)) {
            return;
        }

        $profileEvent = new ProfileEvent($e);
        $profileEvent->renderProfileBox();

        $managementEvent = new UserManagementEvent($e);
        $managementEvent->renderSidebar();

        $rbacEvent = new RbacEvent($e);
        $rbacEvent->setRbacConfig();

        /** @var $authService \Zend\Authentication\AuthenticationService */
        $authService = $e->getApplication()->getServiceManager()->get('ZFS\AuthService');

        if (!$authService->hasIdentity()) {
            $cookie = $e->getApplication()->getRequest()->getCookie();

            if (isset($cookie->rToken) && isset($cookie->rId)) {
                /** @var $cookieAdapter \ZFS\User\Adapter\CookieAdapter */
                $cookieAdapter = $e->getApplication()->getServiceManager()->get('ZFS\CookieAdapter');
                $cookieAdapter->setCookie($cookie->rId, $cookie->rToken);

                $authService->setAdapter($cookieAdapter)->authenticate();
            }
        }

        if ($authService->hasIdentity()) {
            /** @var $userGateway \ZFS\User\Model\Gateway\UserGateway */
            $userGateway = $e->getApplication()->getServiceManager()->get('UserGateway');

            $userRoles = $userGateway->getUserRoles($authService->getIdentity()->userId);

            if (!$userRoles->count()) {
                $authService->clearIdentity();
            }

            $roles = array();
            foreach ($userRoles as $role) {
                $roles[] = $role['role_name'];
            }

            $rbacEvent->setUserRoles($roles);
        } else {
            $rbacEvent->setUserRoles(array(RoleObject::ROLE_GUEST));
        }
    }

    public function getConfig()
    {
        $config = array();

        $configFiles = array(
            __DIR__ . '/config/module.config.php',
            __DIR__ . '/config/module.config.navigation.php',
        );

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = ArrayUtils::merge($config, include $configFile);
        }

        return $config;
    }

    /**
     * Expected to return an array of modules on which the current one depends on
     *
     * @return array
     */
    public function getModuleDependencies()
    {
        return array(/*'ZFS\Common'*/);
    }
}
