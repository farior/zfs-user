zfs-user
================

Модуль регистрации и авторизации для ZFStarter

Подключение
---
Добавить модуль в ```application.config.php```:
```php
'modules' => array(
        'ZFS\User',
        'LfjOpauth',
        ...
        'Application'
    ),
```

Лицензия
----

MIT