<?php

use ZFCTool\Service\Migration\AbstractMigration;

class Migration_20150302_110723_10 extends AbstractMigration
{
    /**
     * Upgrade
     */
    public function up()
    {
        $this->query("DROP TABLE IF EXISTS `mail_templates`");
        $this->query("CREATE TABLE `mail_templates` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `alias` varchar(255) NOT NULL,
          `description` varchar(512) DEFAULT NULL,
          `subject` varchar(255) DEFAULT NULL,
          `bodyHtml` text NOT NULL,
          `bodyText` text NOT NULL,
          `fromEmail` varchar(255) DEFAULT NULL,
          `fromName` varchar(255) DEFAULT NULL,
          `signature` enum('true','false') NOT NULL DEFAULT 'true',
          `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          `updated` timestamp NULL DEFAULT NULL,
          `creator` int(11) NOT NULL,
          `updater` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `mail_templates_unique` (`alias`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8");

        $this->query("INSERT INTO `mail_templates` (`id`, `alias`, `description`, `subject`, `bodyHtml`, `bodyText`, `fromEmail`, `fromName`, `signature`, `created`, `updated`, `creator`, `updater`) VALUES ('1', 'activation_mail', 'mail just after sign up', 'Email confirmation', 'Thank you for registering.. Before we can activate your account one last step must be taken to complete your registration.<br/>Please note - you must complete this last step to become a registered member..You will only need to visit this URL once to activate your account..To complete your registration, please visit this URL:<strong><a href=\"%activationUrl%\">Activate</a></strong><br/>Or insert this URL into your browser: <strong>%activationUrl%</strong><br/>If you accidentally received this email, please visit <strong><a href=\"%cancelUrl%\">cancel link</a></strong>, or just ignore it.', 'Thank you for registering.. Before we can activate your account one last step must be taken to complete your registration. Please note - you must complete this last step to become a registered member.. You will only need to visit this URL once to activate your account.. To complete your registration, please visit this URL:%activationUrl% If you accidentally received this email, please visit %cancelUrl% or just ignore it.', '', '', 'true', '2015-03-16 13:31:43', '0000-00-00 00:00:00', '0', '0')");
        $this->query("INSERT INTO `mail_templates` (`id`, `alias`, `description`, `subject`, `bodyHtml`, `bodyText`, `fromEmail`, `fromName`, `signature`, `created`, `updated`, `creator`, `updater`) VALUES ('2', 'confirm_mail', 'mail just after social auth', 'Social confirmation', 'Confirm your %social% account by clicking <strong><a href=\"%activationUrl%\">Activate</a></strong><br/>Or insert this URL into your browser: <strong>%activationUrl%</strong><br/>If you accidentally received this email, please visit <strong><a href=\"%cancelUrl%\">cancel link</a></strong>, or just ignore it.', 'Confirm your %social% account by insert this URL into your browser: %activationUrl% If you were not supposed to get this letter, ignore it', '', '', 'true', '2015-03-16 13:27:38', '0000-00-00 00:00:00', '0', '0')");
        $this->query("INSERT INTO `mail_templates` (`id`, `alias`, `description`, `subject`, `bodyHtml`, `bodyText`, `fromEmail`, `fromName`, `signature`, `created`, `updated`, `creator`, `updater`) VALUES ('3', 'recovery_mail', 'mail for recovery', 'Recovery confirmation', 'Recovery your %social% account by clicking <strong><a href=\"%activationUrl%\">Activate</a></strong><br/>Or insert this URL into your browser: <strong>%activationUrl%</strong><br/>If you accidentally received this email, please visit <strong><a href=\"%cancelUrl%\">cancel link</a></strong>, or just ignore it.', 'Recovery your %social% account by clicking %activationUrl% If you accidentally received this email, please visit %cancelUrl% or just ignore it.', '', '', 'true', '2015-03-16 13:33:07', '0000-00-00 00:00:00', '0', '0')");
        $this->query("INSERT INTO `mail_templates` (`id`, `alias`, `description`, `subject`, `bodyHtml`, `bodyText`, `fromEmail`, `fromName`, `signature`, `created`, `updated`, `creator`, `updater`) VALUES ('4', 'old_mail_confirm', 'mail for old email confirm', 'Email confirmation', 'From your account, we received a request to change an email, to confirm this action please visit this URL:<strong><a href=\"%activationUrl%\">Activate</a></strong><br/>Or insert this URL into your browser: <strong>%activationUrl%</strong><br/>If you accidentally received this email, please visit <strong><a href=\"%cancelUrl%\">cancel link</a></strong>, or just ignore it.', 'From your account, we received a request to change an email, to confirm this action please visit this URL: %activationUrl% If you accidentally received this email, please visit %cancelUrl% or just ignore it.', '', '', 'true', '2015-03-16 13:35:34', '0000-00-00 00:00:00', '0', '0')");
        $this->query("INSERT INTO `mail_templates` (`id`, `alias`, `description`, `subject`, `bodyHtml`, `bodyText`, `fromEmail`, `fromName`, `signature`, `created`, `updated`, `creator`, `updater`) VALUES ('5', 'new_mail_confirm', 'mail for new email confirm', 'Email confirmation', 'This is the last step to change your account email, to confirm this action please visit this URL:<strong><a href=\"%activationUrl%\">Activate</a></strong><br/>Or insert this URL into your browser: <strong>%activationUrl%</strong><br/>If you accidentally received this email, please visit <strong><a href=\"%cancelUrl%\">cancel link</a></strong>, or just ignore it.', 'This is the last step to change your account email, to confirm this action please visit this URL: %activationUrl% If you accidentally received this email, please visit %cancelUrl% or just ignore it.', '', '', 'true', '2015-03-16 13:36:06', '0000-00-00 00:00:00', '0', '0')");
    }

    /**
     * Degrade
     */
    public function down()
    {
        $this->query("DROP TABLE IF EXISTS `mail_templates`");
    }
}
