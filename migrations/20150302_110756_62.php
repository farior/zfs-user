<?php

use ZFCTool\Service\Migration\AbstractMigration;

class Migration_20150302_110756_62 extends AbstractMigration
{
    /**
     * Upgrade
     */
    public function up()
    {
        $this->query("DROP TABLE IF EXISTS `zfs_users`");
        $this->query("CREATE TABLE `zfs_users` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `email` varchar(255) DEFAULT NULL,
          `name` varchar(255) DEFAULT NULL,
          `avatar` varchar(255) DEFAULT NULL,
          `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
          `status` char(8) NOT NULL DEFAULT 'disabled',
          PRIMARY KEY (`id`),
          UNIQUE KEY `email` (`email`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8");
        $this->query("DROP TABLE IF EXISTS `zfs_auth`");
        $this->query("CREATE TABLE `zfs_auth` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `user_id` int(10) unsigned NOT NULL,
          `provider` varchar(64) NOT NULL,
          `foreign_key` varchar(255) NOT NULL,
          `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
          `expired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
          `status` char(8) NOT NULL DEFAULT 'disabled',
          PRIMARY KEY (`id`),
          UNIQUE KEY `provider` (`provider`,`foreign_key`),
          KEY `user_id` (`user_id`),
          CONSTRAINT `zfs_auth_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `zfs_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8");
        $this->query("DROP TABLE IF EXISTS `zfs_auth_activation`");
        $this->query("CREATE TABLE `zfs_auth_activation` (
          `auth_id` int(10) unsigned NOT NULL,
          `code` varchar(64) NOT NULL,
          `action` varchar(20) NOT NULL,
          `data` text,
          `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `expired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
          UNIQUE KEY `auth_id` (`auth_id`),
          CONSTRAINT `zfs_auth_activation_ibfk_1` FOREIGN KEY (`auth_id`) REFERENCES `zfs_auth` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        $this->query("DROP TABLE IF EXISTS `zfs_auth_credentials`");
        $this->query("CREATE TABLE `zfs_auth_credentials` (
          `auth_id` int(10) unsigned NOT NULL,
          `credential` varchar(128) NOT NULL,
          `expired` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          KEY `auth_id` (`auth_id`),
          CONSTRAINT `zfs_auth_credentials_ibfk_1` FOREIGN KEY (`auth_id`) REFERENCES `zfs_auth` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        $this->query("DROP TABLE IF EXISTS `zfs_users_roles`");
        $this->query("CREATE TABLE `zfs_users_roles` (
          `user_id` int(10) unsigned NOT NULL,
          `role_id` int(10) unsigned NOT NULL,
          PRIMARY KEY (`user_id`,`role_id`),
          KEY `role_id` (`role_id`),
          CONSTRAINT `zfs_users_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `zfs_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT `zfs_users_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `zfs_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->query("INSERT INTO `zfs_users` (`id`, `email`, `name`, `avatar`, `created`, `updated`, `status`) VALUES ('1', 'admin@mail.com', 'admin', '', '2015-03-02 13:11:17', '0000-00-00 00:00:00', 'active')");
        $this->query("INSERT INTO `zfs_auth` (`id`, `user_id`, `provider`, `foreign_key`, `created`, `updated`, `expired`, `status`) VALUES ('1', '1', 'equals', 'admin@mail.com', '2015-03-02 13:11:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active')");
        $this->query("INSERT INTO `zfs_auth_credentials` (`auth_id`, `credential`, `expired`) VALUES ('1', '".'$2y$10$Rkj/NGTiaZ/qcnn8GDgaO.2Yh15p8e4eeWHmYj9iN41zFn.c/wD46'."', '0000-00-00 00:00:00')");
        $this->query("INSERT INTO `zfs_users_roles` (`user_id`, `role_id`) VALUES ('1', '3')");
    }

    /**
     * Degrade
     */
    public function down()
    {
        $this->query("DROP TABLE IF EXISTS `zfs_auth_activation`");
        $this->query("DROP TABLE IF EXISTS `zfs_auth_credentials`");
        $this->query("DROP TABLE IF EXISTS `zfs_auth`");
        $this->query("DROP TABLE IF EXISTS `zfs_users_roles`");
        $this->query("DROP TABLE IF EXISTS `zfs_users`");
    }
}
