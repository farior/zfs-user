<?php

use ZFCTool\Service\Migration\AbstractMigration;

class Migration_20150302_110645_70 extends AbstractMigration
{
    /**
     * Upgrade
     */
    public function up()
    {
        $this->query("INSERT INTO `zfs_privileges` (`id`, `role_id`, `privilege`) VALUES ('1', '1', 'signin')");
        $this->query("INSERT INTO `zfs_privileges` (`id`, `role_id`, `privilege`) VALUES ('2', '1', 'signup')");
        $this->query("INSERT INTO `zfs_privileges` (`id`, `role_id`, `privilege`) VALUES ('3', '2', 'signout')");
        $this->query("INSERT INTO `zfs_privileges` (`id`, `role_id`, `privilege`) VALUES ('5', '3', 'user_management')");
    }

    /**
     * Degrade
     */
    public function down()
    {
    }
}
