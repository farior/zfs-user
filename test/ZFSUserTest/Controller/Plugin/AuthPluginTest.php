<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 06.02.15
 * Time: 15:56
 */

namespace ZFS\User\Test\Controller\Plugin;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\Authentication\Storage\Session;
use ZFS\DomainModel\Object\ObjectMagic;
use ZFS\User\Controller\Plugin\AuthPlugin;
use ZFS\User\Controller\UserController;
use ZFS\User\Model\Object\User;
use ZFS\User\Social\Container\OpauthSocialContainer;
use ZFS\User\Test\Bootstrap;

class AuthPluginTest extends \PHPUnit_Framework_TestCase
{
    /** @var $plugin AuthPlugin */
    protected $plugin;

    /** @var $storage Session */
    protected $storage;

    protected static $dbStorage;

    protected function setUp()
    {
        $serviceManager = clone Bootstrap::getServiceManager();
        $serviceManager->setAllowOverride(true);

        $controller = new UserController();
        $controller->setServiceLocator($serviceManager);

        $this->plugin = new AuthPlugin();
        $this->plugin->setController($controller);

        $this->storage = new Session('ZFSUserTestNamespace');

        $serviceManager->setService('Response', $controller->getResponse());
    }

    protected function setUpMailPlugin()
    {
        $mailPlugin = $this->getMock('ZFS\User\Controller\Plugin\MailPlugin');

        $mailPlugin->expects($this->any())
            ->method('sendActivation')
            ->will($this->returnCallback(function ($auth_id, $action) {
                return $auth_id === 1000;
            }));

        $this->plugin->getController()->getPluginManager()
            ->setService('MailPlugin', $mailPlugin);

        return $this;
    }

    protected function setUpRealAuthService()
    {
        $this->plugin->getController()->getServiceLocator()
            ->setService('ZFS\AuthService', new AuthenticationService(new Session('ZFS\AuthService\Test')));

        return $this;
    }

    protected function setUpAuthService($smAlias, $identity = null)
    {
        $authService = $this->getMock('Zend\Authentication\AuthenticationService');
        $authService->expects($this->any())
            ->method('clearIdentity');

        $authService->expects($this->any())
            ->method('hasIdentity')
            ->will($this->returnValue(isset($identity)));

        $authService->expects($this->any())
            ->method('getIdentity')
            ->will($this->returnValue($identity));

        $authService->expects($this->any())
            ->method('getStorage')
            ->will($this->returnValue($this->storage));

        $this->plugin->getController()->getServiceLocator()->setService($smAlias, $authService);

        return $this;
    }

    protected function setUpSocialAdapter($result = Result::FAILURE, $identity = null)
    {
        $adapter = $this->getMock('ZFS\User\Adapter\SocialAdapter');

        $adapter->expects($this->any())
            ->method('setSocialData');

        $adapter->expects($this->any())
            ->method('authenticate')
            ->will($this->returnCallback(function () use ($identity, $result) {
                return new Result($result, $identity, array('message'));
            }));

        $this->plugin->getController()->getServiceLocator()->setService('ZFS\SocialAdapter', $adapter);

        return $this;
    }

    protected function setUpTableGateway($smAlias, $objectToReturn = null)
    {
        unset(static::$dbStorage[$smAlias]);

        $tableGateway = $this->getMockBuilder('ZFS\Common\Model\Gateway\BaseGateway')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('selectOne')
            ->will($this->returnCallback(function ($where) use ($smAlias, $objectToReturn) {
                if (isset(static::$dbStorage[$smAlias])) {
                    return static::$dbStorage[$smAlias];
                }

                return $objectToReturn;
            }));

        $tableGateway->expects($this->any())
            ->method('delete')
            ->will($this->returnCallback(function ($where) use ($smAlias) {
                unset(static::$dbStorage[$smAlias]);
            }));

        $tableGateway->expects($this->any())
            ->method('insertObject')
            ->will($this->returnCallback(function ($object) use ($smAlias) {
                static::$dbStorage[$smAlias] = $object;
            }));


        $tableGateway->expects($this->any())
            ->method('deleteObject')
            ->will($this->returnCallback(function ($where) use ($smAlias) {
                unset(static::$dbStorage[$smAlias]);
            }));

        $tableGateway->expects($this->any())
            ->method('setCredential');

        $tableGateway->expects($this->any())
            ->method('updateObject')
            ->will($this->returnCallback(function ($object) use ($smAlias) {
                static::$dbStorage[$smAlias] = $object;
            }));

        $this->plugin->getController()->getServiceLocator()->setService($smAlias, $tableGateway);

        return $this;
    }

    public function testGenerateCookie()
    {
        $testedId = 1000;

        $this->setUpTableGateway('AuthGateway')
            ->setUpAuthService('ZFS\AuthService', new ObjectMagic(array(
                'userId' => $testedId,
                'email' => 'some@gmail.com'
            )));

        $this->plugin->generateCookie();

        $cookies = $this->plugin->getController()->getServiceLocator()->get('Response')->getCookie();


        $this->assertEquals('rToken', $cookies[0]->getName());
        $this->assertEquals(32, strlen($cookies[0]->getValue()));
        $this->assertEquals('rId', $cookies[1]->getName());
        $this->assertEquals($testedId, $cookies[1]->getValue());
    }

    /**
     * @param ObjectMagic $auth
     * @param int         $expected
     *
     * @dataProvider generateEqualsDataProvider
     */
    public function testGenerateEquals($auth, $expected)
    {
        $this->setUpTableGateway('AuthGateway', $auth);

        $user = new User(array(
            'id' => 1001,
            'foreign_key' => 'some@gmail.com'
        ));

        $authId = $this->plugin->generateEquals($user, 'qwerty');

        if (!$auth) {
            $this->assertArrayHasKey('AuthGateway', static::$dbStorage);
            $this->assertInstanceOf('ZFS\User\Model\Object\Auth', static::$dbStorage['AuthGateway']);
        }

        $this->assertEquals($expected, $authId);
    }

    public function generateEqualsDataProvider()
    {
        return array(
            array(new ObjectMagic(array('id' => 1000)), 1000),
            array(null, null)
        );
    }


    /**
     * @param string      $socialData
     * @param ObjectMagic $user
     *
     * @dataProvider generateSocialDataProvider
     */
    public function testGenerateSocial($socialData, $user)
    {
        $this->setUpTableGateway('UserGateway', $user)
            ->setUpMailPlugin();

        $socialContainer = new OpauthSocialContainer($socialData);

        $result = $this->plugin->generateSocial($socialContainer);

        $this->assertInstanceOf('ZFS\User\Model\Object\Auth', $result);

        $this->assertArrayHasKey('foreign_key', $result->toArray());
        $this->assertArrayHasKey('provider', $result->toArray());
        $this->assertArrayHasKey('status', $result->toArray());
    }

    public function generateSocialDataProvider()
    {
        return array(
            array(
                base64_encode(serialize(array(
                    'auth' => array(
                        'provider'    => 'Facebook',
                        'uid'         => '12345',
                        'raw'         => '',
                        'credentials' => array('token' => 'qwerty'),
                        'info'        => array('name' => 'user', 'email' => 'some@gmail.com', 'image' => ''))))),
                new ObjectMagic(array('id' => 1000))
            ),
            array(
                base64_encode(serialize(array(
                    'auth' => array(
                        'provider'    => 'Facebook',
                        'uid'         => '12345',
                        'raw'         => '',
                        'credentials' => array('token' => 'qwerty'),
                        'info'        => array('name' => 'user', 'email' => 'some@gmail.com', 'image' => ''))))),
                null
            ),
            array(
                base64_encode(serialize(array(
                    'auth' => array(
                        'provider'    => 'Facebook',
                        'uid'         => '12345',
                        'raw'         => '',
                        'credentials' => array('token' => 'qwerty'),
                        'info'        => array('name' => 'user', 'image' => ''))))),
                null
            )
        );
    }

    /**
     * @param string $socialData
     * @param int    $authResult
     * @param mixed  $expResult
     *
     * @dataProvider authenticateSocialDataProvider
     */
    public function testAuthenticateSocial($socialData, $authResult, $expResult)
    {
        $this->setUpRealAuthService()
            ->setUpSocialAdapter($authResult)
            ->setUpMailPlugin();

        $socialContainer = new OpauthSocialContainer($socialData);

        $result = $this->plugin->authenticateSocial($socialContainer);

        if ($expResult) {
            $this->assertInstanceOf($expResult, $result);
            return;
        }
        $this->assertFalse($expResult);
    }

    public function authenticateSocialDataProvider()
    {
        return array(
            array(
                base64_encode(serialize(array(
                    'auth' => array(
                        'provider'    => 'Facebook',
                        'uid'         => '12345',
                        'raw'         => '',
                        'credentials' => array('token' => 'qwerty'),
                        'info'        => array('name' => 'user', 'image' => ''))))),
                Result::SUCCESS,
                'Zend\Authentication\Result'
            ),
            array(
                base64_encode(serialize(array(
                    'auth' => array(
                        'provider'    => 'Facebook',
                        'uid'         => '12345',
                        'raw'         => '',
                        'credentials' => array('token' => 'qwerty'),
                        'info'        => array('name' => 'user', 'image' => ''))))),
                Result::FAILURE_IDENTITY_NOT_FOUND,
                false
            ),
            array(
                base64_encode(serialize(array(
                    'auth' => array(
                        'provider'    => 'Facebook',
                        'uid'         => '12345',
                        'raw'         => '',
                        'credentials' => array('token' => 'qwerty'),
                        'info'        => array('name' => 'user', 'email' => 'some@gmail.com', 'image' => ''))))),
                Result::FAILURE_IDENTITY_NOT_FOUND,
                'Zend\Authentication\Result'
            ),
        );
    }
}
