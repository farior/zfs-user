<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 30.04.15
 * Time: 16:23
 */

namespace ZFS\User\Test\Controller;

use Zend\Http\Request;
use Zend\Stdlib\ArrayUtils;
use ZFS\DomainModel\Object\ObjectMagic;
use ZFS\User\Controller\ActivationController;
use Zend\Mvc\Router\RouteMatch;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Model\Object\AuthActivation;
use ZFS\User\Test\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;
use ZFS\User\Test\Service\Service;

class ActivationControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @var $controller \ZFS\User\Controller\ActivationController */
    protected $controller;
    /** @var $request \Zend\Http\Request */
    protected $request;
    /** @var $routeMatch \Zend\Mvc\Router\RouteMatch */
    protected $routeMatch;
    /** @var $event \Zend\Mvc\MvcEvent */
    protected $event;
    /** @var $service \ZFS\User\Test\Service\Service */
    protected $service;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();

        $this->controller = new ActivationController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'activation'));
        $this->event      = new MvcEvent();
        $config           = $serviceManager->get('Config');
        $routerConfig     = isset($config['router']) ? $config['router'] : array();
        $router           = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);

        $serviceManager->setAllowOverride(true);

        $serviceManager->setService('Config', ArrayUtils::merge($serviceManager->get('Config'), array(
            'default_routes' => array(
                'redirect' => 'zfsuser'
            )
        )));

        $serviceManager->setService('Response', $this->controller->getResponse());

        $this->service = new Service($this->controller);

        $this->service
            ->setUpIsGrantedPlugin()
            ->setUpAuthService()
            ->setUpSocialStorage();
    }


    /**
     * @param string      $code
     * @param ObjectMagic $activationObject
     * @param string      $expectedResultCode
     *
     * @dataProvider recoveryActionGetDataProvider
     */
    public function testRecoveryActionGet($code, $activationObject, $expectedResultCode)
    {
        $this->service->setUpTableGateway('AuthActivationGateway', $activationObject)
            ->setUpTableGateway('AuthGateway');

        $this->routeMatch->setParam('action', 'recovery');

        if ($code) {
            $this->request->getQuery()->set('code', $code);
        }

        $this->request->setMethod(Request::METHOD_GET);
        $this->controller->dispatch($this->request);

        $this->assertEquals($expectedResultCode, $this->controller->getResponse()->getStatusCode());
    }

    public function recoveryActionGetDataProvider()
    {
        return array(
            array('1q2w3e4r5t6y7u8i9o0p', null, 302),
            array('1q2w3e4r5t6y7u8i9o0p', new ObjectMagic(array('authId' => 1000, 'expired' => '2100-01-01')), 200),
            array(null, new ObjectMagic(array('authId' => 1000, 'expired' => '2100-01-01')), 200),
        );
    }

    /**
     * @param Parameters  $post
     * @param ObjectMagic $auth
     * @param int         $expCode
     * @param string      $code
     * @param ObjectMagic $activation
     * @param ObjectMagic $user
     * @param bool        $mail
     *
     * @dataProvider recoveryActionPostDataProvider
     */
    public function testRecoveryActionPost($post, $auth, $expCode, $code = null, $activation = null, $user = null, $mail = true)
    {
        $this->service->setUpAuthPlugin()
            ->setUpMailPlugin($mail)
            ->setUpTableGateway('AuthActivationGateway', $activation)
            ->setUpTableGateway('AuthGateway', $auth)
            ->setUpTableGateway('UserGateway', $user);

        $this->routeMatch->setParam('action', 'recovery');
        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        if ($code) {
            $this->request->getQuery()->set('code', $code);
        }

        $this->controller->dispatch($this->request);

        $this->assertEquals($expCode, $this->controller->getResponse()->getStatusCode());
    }

    public function recoveryActionPostDataProvider()
    {
        return array(
            array(
                new Parameters(array('password' => 'qwerty', 'password2' => 'qwerty')),
                new ObjectMagic(array('userId' => 1000)),
                302,
                '1q2w3e4r5t6y7u8i9o0p',
                new ObjectMagic(array('authId' => 1000)),
                new ObjectMagic(array('email' => 'some@gmail.com'))
            ),
            array(
                new Parameters(array('password' => 'qwerty', 'password2' => '12345')),
                null,
                200,
                '1q2w3e4r5t6y7u8i9o0p',
                new ObjectMagic(array('authId' => 1000)),
            ),
            array(
                new Parameters(array('email' => 'some@gmail.com')),
                new ObjectMagic(array('id' => 1000,'status' => Auth::STATUS_PENDING)),
                302
            ),
            array(
                new Parameters(array('email' => 'some@gmail.com')),
                null,
                302
            ),
            array(
                new Parameters(array('email' => 'some@gmail.com')),
                new ObjectMagic(array('id' => 1000,'status' => Auth::STATUS_DELETED)),
                302
            ),
            array(
                new Parameters(array('email' => 'some@gmail.com')),
                new ObjectMagic(array('id' => 1000,'status' => Auth::STATUS_DISABLED)),
                302
            ),
            array(
                new Parameters(array('email' => 'some@gmail.com')),
                new ObjectMagic(array('id' => 1000,'status' => Auth::STATUS_ACTIVE)),
                302
            ),
            array(
                new Parameters(array('email' => 'some@gmail.com')),
                new ObjectMagic(array('id' => 1000,'status' => Auth::STATUS_ACTIVE)),
                302,
                null,
                null,
                null,
                false
            ),
            array(
                new Parameters(array('email' => 'some@gmail.com')),
                new ObjectMagic(array('id' => 1001,'status' => Auth::STATUS_ACTIVE)),
                302
            ),
            array(
                new Parameters(array('email' => 'wrong@mail.c123')),
                new ObjectMagic(array('id' => 1001,'status' => Auth::STATUS_ACTIVE)),
                200
            ),
        );
    }

    /**
     * @param string      $code
     * @param string      $action
     * @param ObjectMagic $authActivation
     * @param ObjectMagic $auth
     * @param string      $expLocation
     *
     * @dataProvider testActivationActionDataProvider
     */
    public function testActivationAction($code, $action, $authActivation, $auth, $expLocation)
    {
        $this->service->setUpTableGateway('AuthActivationGateway', $authActivation)
            ->setUpTableGateway('AuthGateway', $auth)
            ->setUpTableGateway('UserGateway')
            ->setUpMailPlugin()
            ->setUpAuthPlugin();

        $this->routeMatch->setParam('action', 'activation');
        $this->request->getQuery()->set('code', $code);
        $this->request->getQuery()->set('action', $action);

        $result = $this->controller->dispatch($this->request);
        $this->assertTrue($result->isRedirect());

        $location = $this->controller->getResponse()->getHeaders()->get('Location')->getUri();

        $this->assertEquals($expLocation, $location);
    }

    public function testActivationActionDataProvider()
    {
        return array(
            array(
                '123123',
                AuthActivation::ACTION_ACTIVATION,
                new ObjectMagic(array(
                    'code' => '123123',
                    'authId' => '1',
                    'expired' => '2025-01-01 00:00:00'
                )),
                new ObjectMagic(array(
                    'status' => Auth::STATUS_ACTIVE,
                    'userId' => '1',
                )),
                "/user"
            ),
            array(
                '123123',
                AuthActivation::ACTION_RECOVERY,
                new ObjectMagic(array(
                    'code' => '123123',
                    'authId' => '1',
                    'expired' => '2025-01-01 00:00:00'
                )),
                new ObjectMagic(array(
                    'status' => Auth::STATUS_ACTIVE,
                    'userId' => '1',
                )),
                '/user/recovery?code=123123'
            ),
            array(
                '123123',
                AuthActivation::ACTION_OLD_EMAIL_CONFIRM,
                new ObjectMagic(array(
                    'code' => '123123',
                    'authId' => '1',
                    'expired' => '2025-01-01 00:00:00'
                )),
                new ObjectMagic(array(
                    'status' => Auth::STATUS_ACTIVE,
                    'userId' => '1',
                )),
                '/user'
            ),
            array(
                '123123',
                AuthActivation::ACTION_NEW_EMAIL_CONFIRM,
                new ObjectMagic(array(
                    'code' => '123123',
                    'authId' => '1',
                    'expired' => '2025-01-01 00:00:00'
                )),
                new ObjectMagic(array(
                    'status' => Auth::STATUS_ACTIVE,
                    'userId' => '1',
                )),
                '/user'
            ),
            array(
                '123123',
                AuthActivation::ACTION_ACTIVATION,
                new ObjectMagic(array(
                    'code' => '123123',
                    'authId' => '1',
                    'expired' => '2015-01-01 00:00:00'
                )),
                new ObjectMagic(array(
                    'status' => Auth::STATUS_ACTIVE,
                    'userId' => '1',
                )),
                "/user"
            ),
            array(
                '123123',
                AuthActivation::ACTION_ACTIVATION,
                null,
                null,
                "/user"
            ),
            array(
                '123123',
                AuthActivation::ACTION_ACTIVATION,
                new ObjectMagic(array(
                    'code' => '123123',
                    'authId' => '1',
                    'expired' => '2015-01-01 00:00:00'
                )),
                null,
                "/user"
            ),

        );
    }


}
