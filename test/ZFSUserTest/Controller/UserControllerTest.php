<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 04.02.15
 * Time: 18:16
 */

namespace ZFS\User\Test\Controller;

use Zend\Authentication\Result;
use Zend\Http\Request;
use Zend\Stdlib\ArrayUtils;
use ZFS\DomainModel\Object\ObjectMagic;
use ZFS\User\Controller\UserController;
use Zend\Mvc\Router\RouteMatch;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Model\Object\Identity;
use ZFS\User\Test\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;
use Zend\Authentication\AuthenticationService;
use ZFS\User\Test\Service\Service;

class UserControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @var $controller \ZFS\User\Controller\UserController */
    protected $controller;
    /** @var $request \Zend\Http\Request */
    protected $request;
    /** @var $routeMatch \Zend\Mvc\Router\RouteMatch */
    protected $routeMatch;
    /** @var $event \Zend\Mvc\MvcEvent */
    protected $event;
    /** @var $service \ZFS\User\Test\Service\Service */
    protected $service;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();

        $this->controller = new UserController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'user'));
        $this->event      = new MvcEvent();
        $config           = $serviceManager->get('Config');
        $routerConfig     = isset($config['router']) ? $config['router'] : array();
        $router           = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);

        $serviceManager->setAllowOverride(true);

        $serviceManager->setService('Config', ArrayUtils::merge($serviceManager->get('Config'), array(
            'default_routes' => array(
                'redirect' => 'zfsuser'
            )
        )));

        $serviceManager->setService('Response', $this->controller->getResponse());

        $this->service = new Service($this->controller);

        $this->service
            ->setUpIsGrantedPlugin()
            ->setUpAuthService()
            ->setUpSocialStorage();
    }

    public function testIndexAction()
    {
        $this->routeMatch->setParam('action', 'index');

        $result = $this->controller->dispatch($this->request);

        $response = $this->controller->getResponse();
        $this->assertInstanceOf('Zend\Http\Response', $result);
        $this->assertTrue($response->isRedirect());
    }

    public function testIndexAuthorizedAction()
    {
        $auth = new ObjectMagic(array(
            'id' => '1',
            'status' => Auth::STATUS_ACTIVE
        ));

        $this->service->setUpTableGateway('AuthGateway', $auth);

        $this->routeMatch->setParam('action', 'index');

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $this->controller->getServiceLocator()->get('ZFS\AuthService');
        $authenticationService->getStorage()->write(new Identity(array(
            'userId' => '1',
            'name' => 'hello',
            'email' => 'world'
        )));

        $result = $this->controller->dispatch($this->request);

        $response = $this->controller->getResponse();
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
        $this->assertTrue($response->isSuccess());

        $authenticationService->getStorage()->clear();
    }



    /**
     * @param Parameters $post
     *
     * @dataProvider testIndexAuthorizedActionPostProvider
     */
    public function testIndexAuthorizedActionPost($post)
    {
        $auth = new ObjectMagic(array(
            'id' => '1',
            'status' => Auth::STATUS_ACTIVE
        ));

        $user = new ObjectMagic();

        $this->service->setUpTableGateway('AuthGateway', $auth)
            ->setUpTableGateway('UserGateway', $user)
            ->setUpMailPlugin()
            ->setUpAuthPlugin()
            ->setUpImagePlugin();

        $this->routeMatch->setParam('action', 'index');

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $this->controller->getServiceLocator()->get('ZFS\AuthService');
        $authenticationService->getStorage()->write(new Identity(array(
            'userId' => '1',
            'name' => 'hello',
            'email' => 'world'
        )));

        $_FILES['avatar']['name'] = 'testName';
        $_FILES['avatar']['tmp_name'] = 'path/to/fixture';

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        $result = $this->controller->dispatch($this->request);

        $response = $this->controller->getResponse();
        $this->assertInstanceOf('Zend\Http\Response', $result);
        $this->assertTrue($response->isRedirect());

        $authenticationService->getStorage()->clear();
    }

    public function testIndexAuthorizedActionPostProvider()
    {
        return array(
            array(
                new Parameters(array(
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'avatar' => array('error' => 0, 'tmp_name' => '/foo', 'name' => 'foo.jpg')
                )),
            ),
        );
    }

    public function testSignoutAction()
    {
        $this->service->setUpAuthPlugin();

        $this->routeMatch->setParam('action', 'signout');

        $result = $this->controller->dispatch($this->request);

        $response = $this->controller->getResponse();
        $this->assertInstanceOf('Zend\Http\Response', $result);
        $this->assertTrue($response->isRedirect());
    }

    public function testSigninActionGet()
    {
        $this->service->setUpEqualsAdapter();

        $this->routeMatch->setParam('action', 'signin');

        $this->request->setMethod(Request::METHOD_GET);
        $result = $this->controller->dispatch($this->request);

        $response = $this->controller->getResponse();
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
        $this->assertTrue($response->isOk());
    }

    /**
     * @param Parameters $post
     * @param int        $authResult
     * @param mixed      $identity
     * @param int        $expectedResultCode
     *
     * @dataProvider signinActionDataProvider
     */
    public function testSigninActionPost($post, $authResult, $identity, $expectedResultCode)
    {
        $this->service->setUpEqualsAdapter($authResult, $identity)
            ->setUpAuthPlugin();

        $this->routeMatch->setParam('action', 'signin');

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);
        $this->controller->dispatch($this->request);

        $response = $this->controller->getResponse();
        $this->assertEquals($expectedResultCode, $response->getStatusCode());
    }

    public function signinActionDataProvider()
    {
        return array(
            array(
                new Parameters(array('email' => 'some@gmail.com', 'password' => 'qwerty', 'rememberMe' => 1)),
                Result::SUCCESS, array(), 302
            ),
            array(
                new Parameters(array('email' => 'bad@email.com', 'password' => 'qwerty', 'rememberMe' => 0)),
                Result::FAILURE, null, 200
            ),
            array(
                new Parameters(array('email' => 'bad@email.com', 'password' => 'qwerty', 'rememberMe' => 0)),
                Result::FAILURE_IDENTITY_NOT_FOUND, null, 200
            ),
            array(
                new Parameters(array('email' => 'bad@email.com', 'password' => 'qwerty', 'rememberMe' => 0)),
                Result::FAILURE_CREDENTIAL_INVALID, null, 200
            ),
        );
    }

    /**
     * @param int         $expCode
     * @param Parameters  $post
     * @param bool        $mailResult
     * @param ObjectMagic $user
     * @param ObjectMagic $auth
     *
     * @dataProvider signupActionPostDataProvider
     */
    public function testSignupActionPost($expCode, $post, $mailResult, $user, $auth)
    {
        $this->service->setUpAuthPlugin()
            ->setUpMailPlugin($mailResult)
            ->setUpTableGateway('UserGateway', $user)
            ->setUpTableGateway('AuthGateway', $auth);

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        $this->routeMatch->setParam('action', 'signup');

        $this->controller->dispatch($this->request);

        $this->assertEquals($expCode, $this->controller->getResponse()->getStatusCode());
    }

    public function signupActionPostDataProvider()
    {
        return array(
            array(302, new Parameters(array(
                    'username' => 'Some User',
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'password2' => 'qwerty',
                )),
                true, new ObjectMagic(array('id' => '1')), null
            ),
            array(302, new Parameters(array(
                    'username' => 'Some User',
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'password2' => 'qwerty',
                )),
                false, new ObjectMagic(array('id' => '1')), null
            ),
            array(200, new Parameters(array(
                    'username' => 'Some User',
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'password2' => 'qwerty',
                )), true, new ObjectMagic(array('id' => '1')),
                new ObjectMagic(array('id' => '1', 'status' => Auth::STATUS_ACTIVE))
            ),
            array(200, new Parameters(array(
                    'username' => 'Some User',
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'password2' => 'qwerty',
                )), true, new ObjectMagic(array('id' => '1')),
                new ObjectMagic(array('id' => '1', 'status' => Auth::STATUS_PENDING))
            ),
            array(302, new Parameters(array(
                    'username' => 'Some User',
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'password2' => 'qwerty',
                )), true, null, null
            ),
            array(
                302,
                new Parameters(array(
                    'username' => 'Some User',
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'password2' => 'qwerty',
                )),
                false,
                null,
                null
            ),
            array(200, new Parameters(array(
                    'username' => 'Some User',
                    'email' => 'some@gmail.com',
                    'password' => 'qwerty',
                    'password2' => 'qwerty2',
                )), false, null, null
            ),
        );
    }




}
