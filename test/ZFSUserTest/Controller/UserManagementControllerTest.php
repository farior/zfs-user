<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 04.03.15
 * Time: 15:48
 */

namespace ZFS\Pages\Test\Controller;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Model\Object\User as UserObject;
use ZFS\User\Test\Bootstrap;
use ZFS\User\Controller\UserManagementController;
use Zend\Mvc\Router\RouteMatch;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Stdlib\Parameters;
use ZFS\User\Test\Service\Service;
use Zend\Db\ResultSet\ResultSet;

class UserManagementControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @var $controller \ZFS\User\Controller\UserManagementController */
    protected $controller;
    /** @var $request \Zend\Http\Request */
    protected $request;
    /** @var $routeMatch \Zend\Mvc\Router\RouteMatch */
    protected $routeMatch;
    /** @var $event \Zend\Mvc\MvcEvent */
    protected $event;
    /** @var $service \ZFS\User\Test\Service\Service */
    protected $service;

    protected static $storage;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();

        $this->controller = new UserManagementController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'usermanagement'));
        $this->event      = new MvcEvent();
        $config           = $serviceManager->get('Config');
        $routerConfig     = isset($config['router']) ? $config['router'] : array();
        $router           = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);

        $serviceManager->setAllowOverride(true);

        $serviceManager->setService('Response', $this->controller->getResponse());

        $this->service = new Service($this->controller);

        $this->service
            ->setUpIsGrantedPlugin()
            ->setUpAuthService()
            ->setUpSocialStorage()
            ->setUpUsersTS();
    }

    /**
     * @param mixed $objectToReturn
     * @return $this
     */
    protected function setUpUserGateway($objectToReturn = null)
    {
        $tableGateway = $this->getMockBuilder('ZFS\User\Model\Gateway\UserGateway')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('getPages')
            ->will($this->returnValue($objectToReturn));

        $tableGateway->expects($this->any())
            ->method('selectOne')
            ->will($this->returnCallback(function ($where) use ($objectToReturn) {
                if (isset($where['id']) && $where['id'] == static::$storage->id) {
                    return static::$storage;
                }

                if (isset($where['email'])) {
                    return $objectToReturn;
                }

                return null;
            }));

        $tableGateway->expects($this->any())
            ->method('delete')
            ->will($this->returnCallback(function ($where) {
                if ($where['id'] == static::$storage->id) {
                    static::$storage = null;
                }
            }));

        $tableGateway->expects($this->any())
            ->method('getAdapter')
            ->will($this->returnCallback(function () {
                return $this->service->getAdapterMock();
            }));

        $tableGateway->expects($this->any())
            ->method('updateObject')
            ->will($this->returnCallback(function ($object) {
                static::$storage = $object;
            }));

        $tableGateway->expects($this->any())
            ->method('insertObject')
            ->will($this->returnCallback(function ($object) {
                static::$storage = $object;
            }));

        $tableGateway->expects($this->any())
            ->method('getUserRoles')
            ->will($this->returnCallback(function () {
                return array(array('role_name' => 'guest'));
            }));

        $tableGateway->expects($this->any())
            ->method('getUsersSql')
            ->will($this->returnCallback(function () {
                return new Select();
            }));

        $this->controller->getServiceLocator()->setService('UserGateway', $tableGateway);

        return $this;
    }

    public function testIndexAction()
    {
        $this->setUpUserGateway();

        $this->routeMatch->setParam('action', 'index');

        $this->controller->dispatch($this->request);

        $this->assertEquals(200, $this->controller->getResponse()->getStatusCode());
    }

    public function testCreateActionNewUser()
    {
        $this->setUpUserGateway();

        $this->service
            ->setUpAuthService()
            ->setUpAuthPlugin();

        $this->routeMatch->setParam('action', 'create');

        $post = new Parameters(array(
            'name' => 'foo',
            'email'  => 'foo@bar.baz',
            'password' => 'foobarbaz',
            'status' => Auth::STATUS_ACTIVE,
            'roles' => array('guest')
        ));

        $expected = new UserObject();
        $expected->fromArray(array(
            'name' => $post['name'],
            'email'  => $post['email'],
            'status' => $post['status'],
        ));

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        $this->controller->dispatch($this->request);

        $this->assertEquals($expected, static::$storage);
        $this->assertEquals(302, $this->controller->getResponse()->getStatusCode());

        static::$storage->id = '1';
    }

    public function testCreateActionUpdateUser()
    {
        $this->setUpUserGateway();

        $this->service
            ->setUpAuthService()
            ->setUpAuthPlugin();

        $this->routeMatch->setParam('action', 'create');

        $post = new Parameters();
        $post->fromArray(static::$storage->toArray());
        $post->set('username', 'buz');
        $post->set('password', 'foobarbaz');
        $post->set('roles', array('guest'));

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        $this->assertNull(static::$storage->updated);

        $this->controller->dispatch($this->request);

        $this->assertEquals(302, $this->controller->getResponse()->getStatusCode());
    }

    public function testCreateActionUserExist()
    {
        $this->setUpUserGateway(true);

        $this->service
            ->setUpAuthService()
            ->setUpAuthPlugin();

        $this->routeMatch->setParam('action', 'create');

        $post = new Parameters();
        $post->fromArray(static::$storage->toArray());
        $post->set('username', 'buz');
        $post->set('password', 'foobarbaz');
        $post->set('roles', array('guest'));

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        $this->controller->dispatch($this->request);

        $this->assertEquals(200, $this->controller->getResponse()->getStatusCode());
    }

    public function testCreateActionInvalidData()
    {
        $this->setUpUserGateway(true);

        $this->service
            ->setUpAuthService()
            ->setUpAuthPlugin();

        $this->routeMatch->setParam('action', 'create');

        $post = new Parameters();
        //$post->fromArray(static::$storage->toArray());
        $post->set('slug', 'buz');
        $post->set('password', 'foobarbaz');
        $post->set('roles', array('guest'));

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);
        $this->controller->dispatch($this->request);

        $this->assertEquals(200, $this->controller->getResponse()->getStatusCode());
    }

    public function testCreateActionEdit()
    {
        $this->setUpUserGateway();

        $this->routeMatch->setParam('action', 'create');
        $this->routeMatch->setParam('id', static::$storage->id);

        $this->request->setMethod(Request::METHOD_GET);

        $this->controller->dispatch($this->request);

        $this->assertEquals(200, $this->controller->getResponse()->getStatusCode());
    }

    public function testDeleteAction()
    {
        $this->setUpUserGateway();

        $this->routeMatch->setParam('action', 'delete');
        $this->routeMatch->setParam('id', static::$storage->id);

        $this->assertNotNull(static::$storage);

        $this->controller->dispatch($this->request);

        $this->assertEquals(302, $this->controller->getResponse()->getStatusCode());

        $this->assertNull(static::$storage);
    }
}
