<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 30.04.15
 * Time: 16:41
 */

namespace ZFS\User\Test\Controller;

use Zend\Http\Request;
use Zend\Stdlib\ArrayUtils;
use ZFS\DomainModel\Object\ObjectMagic;
use Zend\Mvc\Router\RouteMatch;
use ZFS\User\Controller\SocialController;
use ZFS\User\Model\Object\Identity;
use ZFS\User\Social\Container\OpauthSocialContainer;
use ZFS\User\Test\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;
use ZFS\User\Test\Service\Service;
use Zend\Authentication\Result;

class SocialControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @var $controller \ZFS\User\Controller\SocialController */
    protected $controller;
    /** @var $request \Zend\Http\Request */
    protected $request;
    /** @var $routeMatch \Zend\Mvc\Router\RouteMatch */
    protected $routeMatch;
    /** @var $event \Zend\Mvc\MvcEvent */
    protected $event;
    /** @var $service \ZFS\User\Test\Service\Service */
    protected $service;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();

        $this->controller = new SocialController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'social'));
        $this->event      = new MvcEvent();
        $config           = $serviceManager->get('Config');
        $routerConfig     = isset($config['router']) ? $config['router'] : array();
        $router           = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);

        $serviceManager->setAllowOverride(true);

        $serviceManager->setService('Config', ArrayUtils::merge($serviceManager->get('Config'), array(
            'default_routes' => array(
                'redirect' => 'zfsuser'
            )
        )));

        $serviceManager->setService('Response', $this->controller->getResponse());

        $this->service = new Service($this->controller);

        $this->service
            ->setUpIsGrantedPlugin()
            ->setUpSocialStorage()
            ->setUpRedirectPlugin();
    }

    /**
     * @param int         $expCode
     * @param Parameters  $post
     * @param int         $authResult
     * @param ObjectMagic $identity
     *
     * @dataProvider socialCallbackActionDataProvider
     */
    public function testSocialCallbackAction($expCode, $post, $authResult, $identity = null)
    {
        $this->service->setUpSocialStorage()
            ->setUpAuthService()
            ->setUpAuthPlugin($authResult, $identity)
            ->setUpTableGateway('AuthGateway', $identity)
            ->setUpTableGateway('UserGateway');

        /** @var \Zend\Authentication\AuthenticationService $authenticationService */
        $authenticationService = $this->controller->getServiceLocator()->get('ZFS\AuthService');
        $authenticationService->getStorage()->write($identity);


        $this->routeMatch->setParam('action', 'social-callback');

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);
        $result = $this->controller->dispatch($this->request);

        $this->assertInstanceOf('Zend\Http\Response', $result);
        $this->assertEquals($expCode, $this->controller->getResponse()->getStatusCode());
    }

    public function socialCallbackActionDataProvider()
    {
        return array(
            array(
                302,
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'image' => null)
                        ))))
                )),
                Result::SUCCESS
            ),
            array(
                200,
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'image' => null)
                        ))))
                )),
                Result::SUCCESS,
                new ObjectMagic(array())
            ),
            array(
                200,
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'image' => null)
                        ))))
                )),
                Result::FAILURE,
                new ObjectMagic(array())
            ),
            array(
                200,
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'email' => 'some@gmail.com', 'image' => null)
                        ))))
                )),
                Result::SUCCESS
            ),
            array(
                200,
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'email' => 'some@gmail.com', 'image' => null)
                        ))))
                )),
                Result::FAILURE
            ),
            array(
                200,
                new Parameters(array()),
                Result::SUCCESS
            ),
            array(
                200,
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'image' => null)
                        ),
                        'error' => array(
                            'message' => 'some error',
                            'raw'     => 'error'
                        )
                    )))
                )),
                Result::SUCCESS
            ),
        );
    }

    /**
     * @param int        $expCode
     * @param string     $expResponse
     * @param Parameters $post
     *
     * @dataProvider socialConfirmActionDataProvider
     */
    public function testSocialConfirmAction($expCode, $expResponse, $post)
    {
        $this->service->setUpSocialStorage()
            ->setUpAuthService()
            ->setUpAuthPlugin()
            ->setUpTableGateway('AuthGateway');

        $this->routeMatch->setParam('action', 'social-confirm');

        if (isset($post['opauth'])) {
            /** @var $socialStorage \Zend\Authentication\Storage\Session */
            $socialStorage = $this->controller->getServiceLocator()->get('ZFS\SocialStorage');

            $opauthContainer = new OpauthSocialContainer($post['opauth']);

            $socialStorage->write($opauthContainer);
        }

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);
        $result = $this->controller->dispatch($this->request);

        $this->assertInstanceOf($expResponse, $result);
        $this->assertEquals($expCode, $this->controller->getResponse()->getStatusCode());
    }

    public function socialConfirmActionDataProvider()
    {
        return array(
            array(
                200,
                'Zend\Http\Response',
                new Parameters(array(
                )),
            ),
            array(
                200,
                'Zend\Http\Response',
                new Parameters(array(
                )),
            ),
            array(
                200,
                'Zend\Http\Response',
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'image' => ''))))),
                    'email' => 'some@gmail.com'
                )),
            ),
            array(
                200,
                'Zend\View\Model\ViewModel',
                new Parameters(array(
                    'opauth' => base64_encode(serialize(array(
                        'auth' => array(
                            'provider'    => 'Facebook',
                            'uid'         => '12345',
                            'raw'         => '',
                            'credentials' => array('token' => 'qwerty'),
                            'info'        => array('name' => 'user', 'image' => '')
                        )))),
                )),
            ),
        );
    }

}
