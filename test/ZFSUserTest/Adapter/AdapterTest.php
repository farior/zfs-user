<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 06.03.15
 * Time: 12:13
 */

namespace ZFS\User\Test\Adapter;

use Zend\Authentication\Result;
use ZFS\DomainModel\Object\ObjectMagic;
use ZFS\User\Adapter\CookieAdapter;
use ZFS\User\Adapter\EqualsAdapter;
use ZFS\User\Adapter\SocialAdapter;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Social\Container\OpauthSocialContainer;
use ZFS\User\Social\Container\SocialContainer;
use ZFS\User\Test\Bootstrap;
use Zend\Crypt\Password\Bcrypt;

class AdapterTest extends \PHPUnit_Framework_TestCase
{
    protected $serviceLocator;
    /** @var $plugin \ZFS\User\Adapter\CookieAdapter */
    protected $cookieAdapter;
    /** @var $plugin \ZFS\User\Adapter\EqualsAdapter */
    protected $equalsAdapter;
    /** @var $plugin \ZFS\User\Adapter\SocialAdapter */
    protected $socialAdapter;

    protected function setUp()
    {
        $this->serviceLocator = clone Bootstrap::getServiceManager();

        $this->cookieAdapter = new CookieAdapter();
        $this->equalsAdapter = new EqualsAdapter();
        $this->socialAdapter = new SocialAdapter();

        $this->cookieAdapter->setServiceLocator($this->serviceLocator);
        $this->equalsAdapter->setServiceLocator($this->serviceLocator);
        $this->socialAdapter->setServiceLocator($this->serviceLocator);

        $this->serviceLocator->setAllowOverride(true);
    }

    protected function setUpAuthGateway($objectToReturn = null)
    {
        $tableGateway = $this->getMockBuilder('ZFS\User\Model\Gateway\AuthGateway')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('getIdentity')
            ->will($this->returnValue($objectToReturn));

        $this->serviceLocator->setService('AuthGateway', $tableGateway);

        return $this;
    }

    protected function setUpAuthCredentialsGateway($objectToReturn = null)
    {
        $tableGateway = $this->getMockBuilder('ZFS\Common\Model\Gateway\BaseGateway')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('selectOne')
            ->will($this->returnValue($objectToReturn));

        $this->serviceLocator->setService('AuthCredentialsGateway', $tableGateway);

        return $this;
    }

    /**
     * @dataProvider cookieAdapterDataProvider
     */
    public function testCookieAdapter($rId, $rToken, $auth, $authCredential, $resultCode)
    {
        $this->cookieAdapter->setCookie($rId, $rToken);

        $this->assertEquals($rId, \PHPUnit_Framework_Assert::readAttribute($this->cookieAdapter, 'rId'));
        $this->assertEquals($rToken, \PHPUnit_Framework_Assert::readAttribute($this->cookieAdapter, 'rToken'));

        $this->setUpAuthGateway($auth)->setUpAuthCredentialsGateway($authCredential);

        $result = $this->cookieAdapter->authenticate();

        $this->assertInstanceOf('Zend\Authentication\Result', $result);

        $this->assertEquals($resultCode, $result->getCode());
    }

    public function cookieAdapterDataProvider()
    {
        return array(
            array(
                '1', '12345qwerty', null, null, Result::FAILURE_IDENTITY_NOT_FOUND
            ),
            array(
                '1', '12345qwerty',
                new ObjectMagic(array('id' => 1000)),
                new ObjectMagic(array('expired' => '2015-01-01 00:00:00', 'credential' => '12345qwerty')),
                Result::FAILURE
            ),
            array(
                '1', '12345qwerty',
                new ObjectMagic(array('id' => 1000)),
                null,
                Result::FAILURE
            ),
            array(
                '1', '12345qwerty',
                new ObjectMagic(array('id' => 1000)),
                new ObjectMagic(array('expired' => '2025-01-01 00:00:00', 'credential' => '12345qwerty')),
                Result::FAILURE
            ),
            array(
                '1', '12345qwerty',
                new ObjectMagic(array('id' => 1000, 'status' => Auth::STATUS_DELETED)),
                new ObjectMagic(array('expired' => '2025-01-01 00:00:00', 'credential' => '12345qwerty')),
                Result::FAILURE
            ),
            array(
                '1', '12345qwerty',
                new ObjectMagic(array('id' => 1000, 'status' => Auth::STATUS_DISABLED)),
                new ObjectMagic(array('expired' => '2025-01-01 00:00:00', 'credential' => '12345qwerty')),
                Result::FAILURE
            ),
            array(
                '1', '12345qwerty',
                new ObjectMagic(array('id' => 1000, 'authStatus' => Auth::STATUS_PENDING)),
                new ObjectMagic(array('expired' => '2025-01-01 00:00:00', 'credential' => '12345qwerty')),
                Result::FAILURE
            ),
            array(
                '1', '12345qwerty',
                new ObjectMagic(array('id' => 1000, 'authStatus' => Auth::STATUS_ACTIVE)),
                new ObjectMagic(array('expired' => '2025-01-01 00:00:00', 'credential' => '12345qwerty')),
                Result::SUCCESS
            ),

        );
    }

    /**
     * @dataProvider equalsAdapterDataProvider
     */
    public function testEqualsAdapter($auth, $authCredential, $resultCode)
    {
        $this->equalsAdapter->setCredentials('user@mail.com', '12345qwerty');

        $this->assertEquals('user@mail.com', \PHPUnit_Framework_Assert::readAttribute($this->equalsAdapter, 'email'));
        $this->assertEquals('12345qwerty', \PHPUnit_Framework_Assert::readAttribute($this->equalsAdapter, 'password'));

        if ($authCredential) {
            $bCrypt = new Bcrypt();
            $authCredential->credential = $bCrypt->create($authCredential->credential);
        }

        $this->setUpAuthGateway($auth)->setUpAuthCredentialsGateway($authCredential);

        $result = $this->equalsAdapter->authenticate();

        $this->assertInstanceOf('Zend\Authentication\Result', $result);

        $this->assertEquals($resultCode, $result->getCode());
    }

    public function equalsAdapterDataProvider()
    {
        return array(

            array(
                null,
                null,
                Result::FAILURE_IDENTITY_NOT_FOUND
            ),
            array(
                new ObjectMagic(array('id' => 1000, 'authStatus' => Auth::STATUS_ACTIVE)),
                null,
                Result::FAILURE
            ),
            array(
                new ObjectMagic(array('id' => 1000, 'authStatus' => Auth::STATUS_ACTIVE)),
                new ObjectMagic(array('credential' => '12345')),
                Result::FAILURE_CREDENTIAL_INVALID
            ),
            array(
                new ObjectMagic(array('id' => 1000, 'authStatus' => Auth::STATUS_ACTIVE)),
                new ObjectMagic(array('credential' => '12345qwerty')),
                Result::SUCCESS
            ),
        );
    }

    /**
     * @dataProvider socialAdapterDataProvider
     */
    public function testSocialAdapter($auth, $resultCode)
    {
        $socialContainer = new OpauthSocialContainer(
            base64_encode(serialize(array(
                'auth' => array(
                    'provider'    => 'Facebook',
                    'uid'         => '12345',
                    'raw'         => '',
                    'credentials' => array('token' => 'qwerty'),
                    'info'        => array('name' => 'user', 'email' => 'some@gmail.com', 'image' => '')
                ))))
        );

        $this->socialAdapter->setSocialData($socialContainer);

        $this->assertEquals($socialContainer, \PHPUnit_Framework_Assert::readAttribute($this->socialAdapter, 'socialData'));

        $this->setUpAuthGateway($auth);

        $result = $this->socialAdapter->authenticate();

        $this->assertInstanceOf('Zend\Authentication\Result', $result);

        $this->assertEquals($resultCode, $result->getCode());
    }

    public function socialAdapterDataProvider()
    {
        return array(
            array(
                new ObjectMagic(array('id' => 1000, 'authStatus' => Auth::STATUS_ACTIVE)),
                Result::SUCCESS
            ),
            array(
                null,
                Result::FAILURE_IDENTITY_NOT_FOUND
            ),
        );
    }
}
