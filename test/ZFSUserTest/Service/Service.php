<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.03.15
 * Time: 16:27
 */

namespace ZFS\User\Test\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\Authentication\Storage\Session;
use Zend\Db\Sql\Select;

class Service extends \PHPUnit_Framework_TestCase
{
    /** @var $controller \Zend\Mvc\Controller\AbstractActionController */
    protected $controller;

    protected static $storage;

    /**
     * @param \Zend\Mvc\Controller\AbstractActionController $controller
     */
    public function __construct($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param string $smAlias
     * @param mixed $objectToReturn
     *
     * @return $this
     */
    public function setUpTableGateway($smAlias, $objectToReturn = null)
    {
        $tableGateway = $this->getMockBuilder('ZFS\Common\Model\Gateway\BaseGateway')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('selectOne')
            ->will($this->returnCallback(function () use ($objectToReturn) {
                return $objectToReturn;
            }));

        $tableGateway->expects($this->any())
            ->method('delete');
        $tableGateway->expects($this->any())
            ->method('deleteObject');
        $tableGateway->expects($this->any())
            ->method('updateObject');
        $tableGateway->expects($this->any())
            ->method('insertObject');

        $this->controller->getServiceLocator()->setService($smAlias, $tableGateway);

        return $this;
    }

    public function setUpAuthService()
    {
        $this->controller->getServiceLocator()
            ->setService('ZFS\AuthService', new AuthenticationService(new Session('ZFS\AuthService\Test')));

        return $this;
    }

    public function setUpSocialStorage()
    {
        $this->controller->getServiceLocator()
            ->setService('ZFS\SocialStorage', new Session('ZFS\SocialStorage\Test'));

        return $this;
    }

    public function getAdapterMock()
    {
        $driver = $this->getMock('Zend\Db\Adapter\Driver\DriverInterface');

        $adapter = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
            ->disableOriginalConstructor()->getMock();
        ;

        $platform = $this->getMock('Zend\Db\Adapter\Platform\Mysql');

        $stmt = $this->getMock('Zend\Db\Adapter\Driver\Pdo\Statement');

        $paramContainer = $this->getMock('Zend\Db\Adapter\ParameterContainer');

        $platform->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('MySQL'));

        $stmt->expects($this->any())
            ->method('getParameterContainer')
            ->will($this->returnValue($paramContainer));

        $stmt->expects($this->any())
            ->method('setSql')
            ->will($this->returnValue($stmt));

        $resultSet = $this->getMock('Zend\Db\ResultSet\ResultSet');
        $resultSet->expects($this->any())
            ->method('current')
            ->will($this->returnValue(array('c' => '0')));

        $stmt->expects($this->any())
            ->method('execute')
            ->will($this->returnValue($resultSet));

        $adapter->expects($this->any())
            ->method('getPlatform')
            ->will($this->returnValue($platform));


        $driver->expects($this->any())
            ->method('createStatement')
            ->will($this->returnValue($stmt));

        $adapter->expects($this->any())
            ->method('getDriver')
            ->will($this->returnValue($driver));

        return $adapter;
    }

    /**
     * @param int   $result
     * @param mixed $identity
     *
     * @return $this
     */
    public function setUpSocialAdapter($result = Result::FAILURE, $identity = null)
    {
        $adapter = $this->getMock('ZFS\User\Adapter\SocialAdapter');

        $adapter->expects($this->any())
            ->method('setSocialData');

        $adapter->expects($this->any())
            ->method('authenticate')
            ->will($this->returnCallback(function () use ($identity, $result) {
                return new Result($result, $identity, array('message'));
            }));

        $this->controller->getServiceLocator()->setService('ZFS\SocialAdapter', $adapter);

        return $this;
    }

    /**
     * @param int   $result
     * @param mixed $identity
     *
     * @return $this
     */
    public function setUpEqualsAdapter($result = Result::FAILURE, $identity = null)
    {
        $adapter = $this->getMock('ZFS\User\Adapter\EqualsAdapter');

        $adapter->expects($this->any())
            ->method('setCredentials');

        $adapter->expects($this->any())
            ->method('authenticate')
            ->will($this->returnCallback(function () use ($identity, $result) {
                return new Result($result, $identity, array('message'));
            }));

        $this->controller->getServiceLocator()->setService('ZFS\EqualsAdapter', $adapter);

        return $this;
    }

    public function setUpUsersTS()
    {
        $usersTS = $this->getMock('ZFS\User\Model\TransactionScript\Users');
        $usersTS->expects($this->any())
            ->method('select')
            ->will($this->returnCallback(function () {
                return new Select();
            }));

        $this->controller->getServiceLocator()->setService('UsersTransactionScript', $usersTS);

        return $this;
    }

    public function setUpControllerPluginManager()
    {
        $controller = $this->controller;

        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager');
        $pluginManager->expects($this->any())
            ->method('get')
            ->will($this->returnCallback(function ($pluginName) use ($controller) {
                $plugin = $controller->getPluginManager()->get($pluginName);
                $plugin->setController($controller);
                return $plugin;
            }));

        $this->controller->getPluginManager()->setService('controllerpluginmanager', $pluginManager);

        return $this;
    }

    /**
     * @param int   $result
     * @param mixed $identity
     *
     * @return $this
     */
    public function setUpAuthPlugin($result = Result::SUCCESS, $identity = null)
    {
        $authPlugin = $this->getMock('ZFS\User\Controller\Plugin\AuthPlugin');

        $authPlugin->expects($this->any())
            ->method('generateCookie');

        $authPlugin->expects($this->any())
            ->method('removeCookie');

        $authPlugin->expects($this->any())
            ->method('generateEquals')
            ->will($this->returnValue(1));

        $authPlugin->expects($this->any())
            ->method('authenticateSocial')
            ->will($this->returnCallback(function () use ($result, $identity) {
                return new Result($result, $identity, array('message'));
            }));

        $authPlugin->expects($this->any())
            ->method('checkSocial')
            ->will($this->returnCallback(function () use ($identity) {
                return isset($identity);
            }));

        $this->controller->getPluginManager()->setService('AuthPlugin', $authPlugin);

        return $this;
    }

    public function setUpRedirectPlugin()
    {
        /** @var \Zend\Mvc\Controller\Plugin\Redirect $redirectPlugin */
        $redirectPlugin = $this->controller->getServiceLocator()->get('controllerpluginmanager')->get('redirect');
        $redirectPlugin->setController($this->controller);

        $this->controller->getPluginManager()->setService('redirect', $redirectPlugin);

        return $this;
    }

    public function setUpMailPlugin($result = true)
    {
        $mailPlugin = $this->getMock('ZFS\User\Controller\Plugin\MailPlugin');

        $mailPlugin->expects($this->any())
            ->method('sendActivation')
            ->will($this->returnCallback(function () use ($result) {
                return $result;
            }));

        $this->controller->getPluginManager()->setService('MailPlugin', $mailPlugin);

        return $this;
    }

    public function setUpImagePlugin()
    {
        $imagePlugin = $this->getMock('ZFS\User\Controller\Plugin\ImagePlugin');

        $imagePlugin->expects($this->any())
            ->method('saveAvatar');

        $this->controller->getPluginManager()->setService('ImagePlugin', $imagePlugin);

        return $this;
    }

    /**
     * @return $this
     */
    public function setUpIsGrantedPlugin()
    {
        $isGranted = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\AbstractPlugin')
            ->disableOriginalConstructor()
            ->getMock();

        $isGranted->expects($this->any())
            ->method('__invoke');

        $this->controller->getPluginManager()
            ->setInvokableClass('isGranted', get_class($isGranted));

        return $this;
    }

}
