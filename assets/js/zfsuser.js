/**
 * Created by dev on 13.03.15.
 */
// Asking user about deleting record
$('body').on('click', '.btn-remove-social', function () {
    return window.confirm("Are you sure you want to remove this social authorization?");
});

$('.btn-social').click(function () {
    $(this).addClass('disabled');
});

$('.avatar').click(function(){
    $('input[name="avatar"]').trigger('click');
});