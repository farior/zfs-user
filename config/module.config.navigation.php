<?php
/**
 * User: dev
 * Date: 25.02.15 11:33
 */
return array(
    'navigation' => array(
        'default' => array(
            array(
                'label'      => 'Sign In',
                'title'      => 'Sign In',
                'module'     => 'ZFS\User',
                'route'      => 'zfsuser/signin',
                'permission' => 'signin',
            ),
            array(
                'label'      => 'Sign Up',
                'title'      => 'Sign Up',
                'module'     => 'ZFS\User',
                'route'      => 'zfsuser/signup',
                'permission' => 'signin',
            ),
            array(
                'class'      => 'dropdown user user-menu',
                'event'      => 'ZFS\User\Event\Navigation\Render\ProfileBox',
                'route'      => 'zfsuser/signout',
                'title'      => 'Profile',
                'permission' => 'signout',
                'order'      => 100,
                'pages'      => array(
                    array(
                        'label'      => 'Profile',
                        'title'      => 'Profile',
                        'module'     => 'ZFS\User',
                        'route'      => 'zfsuser',
                        'tag'        => 'profile_button',
                        'visible'    => false
                    ),
                    array(
                        'label'      => 'Sign Out',
                        'title'      => 'Sign Out',
                        'module'     => 'ZFS\User',
                        'route'      => 'zfsuser/signout',
                        'tag'        => 'signout_button',
                        'visible'    => false
                    )
                )
            )
        )
    ),
);
