<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 13:10
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'ZFS\User\Controller\User' => 'ZFS\User\Controller\UserController',
            'ZFS\User\Controller\Social' => 'ZFS\User\Controller\SocialController',
            'ZFS\User\Controller\Activation' => 'ZFS\User\Controller\ActivationController',
            'ZFS\User\Controller\UserManagement' => 'ZFS\User\Controller\UserManagementController'
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'AuthPlugin' => 'ZFS\User\Controller\Plugin\AuthPlugin',
            'MailPlugin' => 'ZFS\User\Controller\Plugin\MailPlugin',
            'MessagePlugin' => 'ZFS\User\Controller\Plugin\MessagePlugin',
            'ImagePlugin' => 'ZFS\User\Controller\Plugin\ImagePlugin',
        )
    ),
    'router' => array(
        'routes' => array(
            'zfsusermanagement' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/management/user',
                    'defaults' => array(
                        'controller' => 'ZFS\User\Controller\UserManagement',
                        'action'     => 'index',
                        'layout'     => 'layout/dashboard'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'create' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/create',
                            'defaults' => array(
                                'action' => 'create',
                                'layout' => 'layout/dashboard'
                            ),
                        ),
                    ),
                    'edit' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/edit[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]*'
                            ),
                            'defaults' => array(
                                'action' => 'create',
                                'layout' => 'layout/dashboard'
                            )
                        )
                    ),
                    'delete' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/delete[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]*'
                            ),
                            'defaults' => array(
                                'action' => 'delete'
                            )
                        )
                    )
                )
            ),
            'zfsuser' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/user',
                    'defaults' => array(
                        'controller' => 'ZFS\User\Controller\User',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'signin' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/signin',
                            'defaults' => array(
                                'action' => 'signin'
                            ),
                        ),
                    ),
                    'signout' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/signout',
                            'defaults' => array(
                                'action' => 'signout'
                            ),
                        ),
                    ),
                    'signup' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/signup',
                            'defaults' => array(
                                'action' => 'signup'
                            ),
                        ),
                    ),
                    'recovery' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/recovery',
                            'defaults' => array(
                                'controller' => 'ZFS\User\Controller\Activation',
                                'action' => 'recovery'
                            ),
                        ),
                    ),
                    'activation' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/activation',
                            'defaults' => array(
                                'controller' => 'ZFS\User\Controller\Activation',
                                'action' => 'activation'
                            ),
                        ),
                    ),
                    'socialCallback' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/social-callback',
                            'defaults' => array(
                                'controller' => 'ZFS\User\Controller\Social',
                                'action' => 'socialCallback'
                            ),
                        ),
                    ),
                    'socialConfirm' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/social-confirm',
                            'defaults' => array(
                                'controller' => 'ZFS\User\Controller\Social',
                                'action' => 'socialConfirm'
                            ),
                        ),
                    ),
                    'socialRemove' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/social-remove[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]*'
                            ),
                            'defaults' => array(
                                'controller' => 'ZFS\User\Controller\Social',
                                'action' => 'socialRemove'
                            ),
                        ),
                    ),
                    'socialLogin' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/social-login/[:provider[/:oauth_callback]]',
                            'constraints' => array(
                                'provider'  => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'oauth_callback' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array(
                                'controller' => 'ZFS\User\Controller\Social',
                                'action' => 'socialLogin'
                            ),
                        ),
                    ),
                ),
            )
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'template_path_stack' => array(
            __DIR__ . '/../view'
        ),
    ),
    'view_helpers' => array(
        'invokables'=> array(
            'formMulticheckboxWrapper' => 'ZFS\User\View\Helper\FormMultiCheckboxWrapped',
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'ZFS\DomainModel\Service' => 'ZFS\DomainModel\Service\Factory',
            'ZFS\DomainModel\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'ZFS\SessionStorage' => 'ZFS\User\Services\Factory\SessionStorageFactory',
            'ZFS\AuthService' => 'ZFS\User\Services\Factory\AuthServiceFactory',
            'ZFS\SocialStorage' => 'ZFS\User\Services\Factory\SocialStorageFactory',
        ),
        'abstract_factories' => array(
            'ZFS\User\Model\Gateway\AbstractFactory'
        ),
        'aliases' => array(
            'Zend\Authentication\AuthenticationService' => 'ZFS\AuthService',
        ),
        'invokables' => array(
            'HasEmailStrategy' => 'ZFS\User\Social\Strategy\HasEmailStrategy',
            'HasNoEmailStrategy' => 'ZFS\User\Social\Strategy\HasNoEmailStrategy',
            'ZFS\SocialLogin' => 'ZFS\User\Social\SocialLogin',
            'ZFS\SocialAdapter' => 'ZFS\User\Adapter\SocialAdapter',
            'ZFS\CookieAdapter' => 'ZFS\User\Adapter\CookieAdapter',
            'ZFS\EqualsAdapter' => 'ZFS\User\Adapter\EqualsAdapter',
            'SocialContainer' => 'ZFS\User\Social\Container\SocialContainer',
            'UsersTransactionScript' => 'ZFS\User\Model\TransactionScript\Users',
            'ZFS\Service\Auth' => 'ZFS\User\Services\AuthService',
            'ZFS\Service\Image' => 'ZFS\User\Services\ImageService',
            'ZFS\Service\Mail' => 'ZFS\User\Services\MailService',
        )
    ),
    'avatar' => array(
        'size' => array(
            'height' => 200,
            'width'  => 200
        ),
        'save_folder' => 'avatar',
        'quality' => 95
    ),
    'social_email_required' => true,//ask whether the user about entering an email if it hasn't provided in social data
    'default_routes' => array(
        'redirect' => 'home',
        'login_redirect' => 'zfsuser',
        'social_email_confirm' => 'zfsuser/socialConfirm', // only works with 'social_email_required' => true
        'social_login_error' => 'zfsuser/signin'
    ),
    'flash_messages' => array(
        'not_found'          => 'Account %email% was not found',
        'pending'            => 'Account %email% is pending activation',
        'deleted'            => 'Account %email% has been deleted',
        'inactive'           => 'Account %email% is inactive',
        'email_exist'        => 'Email %email% is already taken',
        'mail_error'         => 'Sorry, we can\'t send you a letter ;(',
        'recovery'           => 'We\'ve sent an email to %email%. Open it up to recovery your account',
        'activation_expired' => 'The activation code has expired',
        'activation_success' => 'Congratulations, you can now log in',
        'signup_success'     => 'Almost done... We\'ve sent an email to %email%. Open it up to activate your account',
        'signup_mail_exist'  => 'There is already an account with similar Email address.'.
                                ' To continue please confirm your Email. Activation link sent to %email%',
        'password_changed'   => 'Password changed for account %email%',
        'password_short'     => 'Password must be at least 5 symbols',
        'social_confirm'     => 'We\'ve sent you an email to %email%. Open it up to activate your account',
        'old_email_confirm'  => 'We\'ve sent an email to %email%. Open it up to change your email',
        'new_email_confirm'  => 'We\'ve sent an email to %email%. Open it up to confirm your email',
        'email_changed'      => 'Congratulations, you have successfully changed the email',
        'page_exist'         => 'Page with alias "%alias%" already exist'
    )
);
