<?php

namespace ZFS\User\EventManager;

use Zend\EventManager\EventManagerAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Mvc\MvcEvent;
use ZFS\Rbac\Rbac as RbacService;

class Rbac implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    protected $mvcEvent;

    public function __construct(MvcEvent $e)
    {
        $this->mvcEvent = $e;
        $this->setEventManager($this->mvcEvent->getApplication()->getEventManager());
    }

    public function setRbacConfig()
    {
        $this->getEventManager()->getSharedManager()->attach(
            RbacService::EVENT_MANAGER_IDENTIFIER,
            RbacService::EVENT_GET_CONFIG,
            function () {
                return $this->getRbacConfig();
            }
        );
    }

    public function setUserRoles($roles)
    {
        $this->getEventManager()->getSharedManager()->attach(
            RbacService::EVENT_MANAGER_IDENTIFIER,
            RbacService::EVENT_GET_USER_ROLES,
            function () use ($roles) {
                return $roles;
            }
        );
    }

    /**
     * Get Rbac config from DB
     *
     * @return array
     */
    protected function getRbacConfig()
    {
        /** @var $roleGateway \ZFS\User\Model\Gateway\RoleGateway */
        $roleGateway = $this->mvcEvent->getApplication()->getServiceManager()->get('RoleGateway');

        $roles = $roleGateway->getRoles();

        $rbac  = array();

        foreach ($roles as $role) {
            $roleItem = array();

            if (!empty($role['privileges'])) {
                $roleItem['permissions'] = explode(',', $role['privileges']);
            }
            if (!empty($role['children'])) {
                $roleItem['children'] = explode(',', $role['children']);
            }
            if (!empty($roleItem)) {
                $rbac[$role['name']] = $roleItem;
            }
        }

        return $rbac;
    }
}
