<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.04.15
 * Time: 18:26
 */

namespace ZFS\User\EventManager;

use Zend\EventManager\EventManagerAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Mvc\MvcEvent;

class Profile implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    protected $mvcEvent;

    public function __construct(MvcEvent $e)
    {
        $this->mvcEvent = $e;
        $this->setEventManager($this->mvcEvent->getApplication()->getEventManager());
    }

    public function renderProfileBox()
    {
        $this->getEventManager()->getSharedManager()->attach(
            'ZFS\Event\Navigation',
            'ZFS\User\Event\Navigation\Render\\'. \ZFS\User\Module::PROFILE_BOX_RENDER,
            function ($event) {
                if (!$this->mvcEvent->getRouteMatch()) {
                    return null;
                }

                /** @var $forward \Zend\Mvc\Controller\Plugin\Forward */
                $forward = $this->mvcEvent->getApplication()->getServiceManager()->get('ControllerPluginManager')->get('forward');

                /** @var $viewManager \Zend\Mvc\View\Http\ViewManager  */
                $viewManager = $this->mvcEvent->getApplication()->getServiceManager()->get('ViewManager');

                $viewManager->getRenderer()->render(
                    $forward->dispatch('ZFS\User\Controller\User', array(
                        'action' => 'profileBox',
                        'params' => array_merge(
                            $event->getParams(),
                            array('placeholder' => \ZFS\User\Module::PROFILE_BOX_RENDER)
                        )))
                );

                /** @var $placeholder \Zend\View\Helper\Placeholder */
                $placeholder = $this->mvcEvent->getApplication()->getServiceManager()->get('viewhelpermanager')->get('placeholder');

                $html = $placeholder->getContainer(\ZFS\User\Module::PROFILE_BOX_RENDER)->getValue();

                return is_string($html) ? $html : '';
            }
        );
    }
}
