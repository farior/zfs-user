<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.04.15
 * Time: 18:35
 */

namespace ZFS\User\EventManager;

use Zend\EventManager\EventManagerAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Mvc\MvcEvent;

class UserManagement implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    protected $mvcEvent;

    public function __construct(MvcEvent $e)
    {
        $this->mvcEvent = $e;
        $this->setEventManager($this->mvcEvent->getApplication()->getEventManager());
    }

    public function renderSidebar()
    {
        $this->getEventManager()->getSharedManager()->attach(
            'ZFS\Dashboard\Event',
            'ZFS\Dashboard\Event\Navigation\Sidebar',
            function () {
                /** @var $forward \Zend\Mvc\Controller\Plugin\Forward */
                $forward = $this->mvcEvent->getApplication()->getServiceManager()->get('ControllerPluginManager')->get('forward');

                /** @var $viewManager \Zend\Mvc\View\Http\ViewManager  */
                $viewManager = $this->mvcEvent->getApplication()->getServiceManager()->get('ViewManager');

                $viewManager->getRenderer()->render(
                    $forward->dispatch('ZFS\User\Controller\UserManagement', array('action' => 'navigation'))
                );
            }
        );
    }
}
