<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 11.02.15
 * Time: 13:03
 */

namespace ZFS\User\Social;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class SocialLogin implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @param \Zend\Stdlib\Parameters $postData
     * @return \ZFS\User\Social\SocialStrategy
     */
    public function getStrategy($postData)
    {
        if (isset($postData['opauth'])) {
            $socialData = unserialize(base64_decode($postData['opauth']));

            /** @var $authService \Zend\Authentication\AuthenticationService */
            $authService = $this->getServiceLocator()->get('ZFS\AuthService');

            if (isset($socialData['auth']['info']['email']) || $authService->hasIdentity()) {
                return $this->invokeStrategy('HasEmailStrategy');
            }
        }

        return $this->invokeStrategy('HasNoEmailStrategy');
    }

    /**
     * @param string $smAlias
     * @return \ZFS\User\Social\SocialStrategy
     */
    protected function invokeStrategy($smAlias)
    {
        /** @var $strategy \ZFS\User\Social\SocialStrategy */
        $strategy = $this->getServiceLocator()->get($smAlias);
        $strategy->setServiceLocator($this->getServiceLocator());

        return $strategy;
    }
}
