<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 11.02.15
 * Time: 12:15
 */

namespace ZFS\User\Social\Strategy;

use Zend\Authentication\Result;
use ZFS\User\Form\Base as BaseForm;
use ZFS\User\Social\SocialStrategy;
use Zend\View\Model\ViewModel;

class HasNoEmailStrategy extends SocialStrategy
{
    /**
     * @param \ZFS\User\Social\Container\SocialContainerInterface $socialData
     * @return \Zend\Http\Response
     */
    public function preProcess($socialData)
    {
        $config = $this->getServiceLocator()->get('Config');

        /** @var $redirectPlugin \Zend\Mvc\Controller\Plugin\Redirect */
        $redirectPlugin = $this->getControllerPlugin('redirect');

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $this->getServiceLocator()->get('ZFS\Service\Auth');

        if ($socialData->hasProvidedEmail() || !$config['social_email_required']) {
            $socialData->setProvidedEmail();
            $authService->authenticateSocial($socialData);

            return $redirectPlugin->toRoute($this->getRoutesConfig()->login_redirect);
        }

        if (!$authService->checkSocial($socialData)) {
            /** @var $socialStorage \Zend\Authentication\Storage\Session */
            $socialStorage = $this->getServiceLocator()->get('ZFS\SocialStorage');
            $socialStorage->write($socialData);

            return $redirectPlugin->toRoute($this->getRoutesConfig()->social_email_confirm);
        }

        // try to authenticate social
        $authResult = $authService->authenticateSocial($socialData);

        /** @var $flashMessenger \Zend\Mvc\Controller\Plugin\FlashMessenger */
        $flashMessenger = $this->getControllerPlugin('flashmessenger');

        if ($authResult->getCode() !== Result::SUCCESS) {
            foreach ($authResult->getMessages() as $message) {
                $flashMessenger->addWarningMessage($message);
            }

            return $redirectPlugin->toRoute($this->getRoutesConfig()->social_login_error);
        }

        return $redirectPlugin->toRoute($this->getRoutesConfig()->login_redirect);
    }

    /**
     * @param \Zend\Stdlib\Parameters $postData
     * @return ViewModel
     */
    public function postProcess($postData)
    {
        $socialForm = new BaseForm('social');

        if ($postData->get('email')) {
            $socialForm->setData($postData);

            if ($socialForm->isValid()) {
                $data = $socialForm->getData();

                /** @var $socialStorage \Zend\Authentication\Storage\Session */
                $socialStorage = $this->getServiceLocator()->get('ZFS\SocialStorage');

                /** @var $socialData \ZFS\User\Social\Container\SocialContainerInterface */
                $socialData = $socialStorage->read();

                $socialData->email = $data['email'];

                /** @var $authService \ZFS\User\Services\AuthService */
                $authService = $this->getServiceLocator()->get('ZFS\Service\Auth');

                $authResult = $authService->authenticateSocial($socialData);

                /** @var $flashMessenger \Zend\Mvc\Controller\Plugin\FlashMessenger */
                $flashMessenger = $this->getControllerPlugin('flashmessenger');

                if (!$authResult) {
                    $flashMessenger->addInfoMessage('social_confirm', array('email' => $data['email']));
                } elseif ($authResult->getCode() !== Result::SUCCESS) {
                    foreach ($authResult->getMessages() as $message) {
                        $flashMessenger->addWarningMessage($message);
                    }
                }

                $socialStorage->clear();
            }
        }

        return new ViewModel(array(
            'form' => $socialForm
        ));
    }
}
