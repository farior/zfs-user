<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 11.02.15
 * Time: 12:14
 */

namespace ZFS\User\Social\Strategy;

use Zend\Authentication\Result;
use ZFS\User\Social\SocialStrategy;

class HasEmailStrategy extends SocialStrategy
{
    /**
     * @param \ZFS\User\Social\Container\SocialContainerInterface $socialData
     * @return \Zend\Http\Response
     */
    public function preProcess($socialData)
    {
        /** @var $flashMessenger \Zend\Mvc\Controller\Plugin\FlashMessenger */
        $flashMessenger = $this->getControllerPlugin('flashmessenger');

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $this->getServiceLocator()->get('ZFS\Service\Auth');

        /** @var $redirectPlugin \Zend\Mvc\Controller\Plugin\Redirect */
        $redirectPlugin = $this->getControllerPlugin('redirect');

        /** @var $authenticationService \Zend\Authentication\AuthenticationService */
        $authenticationService = $this->getServiceLocator()->get('ZFS\AuthService');

        if ($authenticationService->hasIdentity()) {
            $authService->linkSocial($socialData);

            return $redirectPlugin->toRoute($this->getRoutesConfig()->login_redirect);
        }

        $authResult = $authService->authenticateSocial($socialData);

        if ($authResult->getCode() !== Result::SUCCESS) {
            foreach ($authResult->getMessages() as $message) {
                $flashMessenger->addWarningMessage($message);
            }

            return $redirectPlugin->toRoute($this->getRoutesConfig()->social_login_error);
        }

        return $redirectPlugin->toRoute($this->getRoutesConfig()->login_redirect);
    }

    /**
     * @param \Zend\Stdlib\Parameters $postData
     * @return \Zend\View\Model\ViewModel
     */
    public function postProcess($postData)
    {
    }
}
