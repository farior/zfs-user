<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 11.02.15
 * Time: 16:00
 */

namespace ZFS\User\Social;

interface SocialStrategyInterface
{
    /**
     * @param \ZFS\User\Social\Container\SocialContainerInterface $socialData
     */
    public function preProcess($socialData);

    /**
     * @param \Zend\Stdlib\Parameters $postData
     */
    public function postProcess($postData);
}
