<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 11.02.15
 * Time: 12:14
 */

namespace ZFS\User\Social;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class SocialStrategy implements SocialStrategyInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @param \ZFS\User\Social\Container\SocialContainerInterface $socialData
     * @return \Zend\Http\Response
     */
    abstract public function preProcess($socialData);

    /**
     * @param \Zend\Stdlib\Parameters $postData
     * @return \Zend\View\Model\ViewModel
     */
    abstract public function postProcess($postData);

    /**
     * @param string $smAlias
     * @return \Zend\Mvc\Controller\Plugin\AbstractPlugin
     */
    protected function getControllerPlugin($smAlias)
    {
        return $this->getServiceLocator()->get('controllerpluginmanager')->get($smAlias);
    }

    /**
     * @return object
     */
    protected function getRoutesConfig()
    {
        return (object)$this->getServiceLocator()->get('Config')['default_routes'];
    }
}
