<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 12.02.15
 * Time: 15:31
 */

namespace ZFS\User\Social\Container;

/**
 * @property string $provider
 * @property string $uid
 * @property string $token
 * @property string $name
 * @property string $image
 * @property string $email
 * @property string $expired
 * @property array  $error
 */
class OpauthSocialContainer implements SocialContainerInterface
{
    protected $providedEmail = false;
    protected $hasError      = false;

    protected $rawData = null;

    /**
     * @param string $opAuthPostData
     */
    public function __construct($opAuthPostData)
    {
        $opAuthData = unserialize(base64_decode($opAuthPostData));

        if (empty($opAuthData)) {
            throw new \InvalidArgumentException(
                'Constructor argument provided must be response of opAuth post callback'
            );
        }

        if (isset($opAuthData['error'])) {
            $this->hasError = true;

            $this->error    = $opAuthData['error']['message'] ? : 'Unexpected error, check config';
            $this->rawData  = $opAuthData['error']['raw'];
        }

        if (isset($opAuthData['auth'])) {
            $this->rawData  = $opAuthData['auth']['raw'];
            $this->provider = strtolower($opAuthData['auth']['provider']);
            $this->uid      = $opAuthData['auth']['uid'];
            $this->token    = $opAuthData['auth']['credentials']['token'];
            $this->name     = $opAuthData['auth']['info']['name'];
            $this->image    = $opAuthData['auth']['info']['image'] ? : null;

            $this->providedEmail = isset($opAuthData['auth']['info']['email']);
            $this->email    =  $this->providedEmail ? $opAuthData['auth']['info']['email'] : null;

            $this->expired  = isset($opAuthData['auth']['credentials']['expires']) ?
                gmdate('Y-m-d H:i:s', strtotime($opAuthData['auth']['credentials']['expires'])) :
                gmdate('Y-m-d H:i:s', strtotime('now'));
        }
    }

    /**
     * @return bool
     */
    public function hasProvidedEmail()
    {
        return $this->providedEmail;
    }

    public function setProvidedEmail()
    {
        $this->providedEmail = true;
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return $this->hasError;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        if ($this->hasError()) {
            return $this->error;
        }

        return '';
    }
}
