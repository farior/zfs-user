<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.02.15
 * Time: 18:44
 */

namespace ZFS\User\Social\Container;

interface SocialContainerInterface
{
    /**
     * @return bool
     */
    public function hasProvidedEmail();

    public function setProvidedEmail();

    /**
     * @return bool
     */
    public function hasError();

    /**
     * @return string
     */
    public function getErrorMessage();
}
