<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.02.15
 * Time: 19:00
 */

namespace ZFS\User\Social\Container;

class SocialContainer
{
    /**
     * @param \Zend\Stdlib\Parameters $postData
     * @return SocialContainerInterface
     */
    public function get($postData)
    {
        if (isset($postData['opauth'])) {
            return new OpauthSocialContainer($postData['opauth']);
        }

        return null;
    }
}
