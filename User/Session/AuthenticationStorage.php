<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 09.02.15
 * Time: 17:15
 */

namespace ZFS\User\Session;

use Zend\Authentication\Storage\Session;
use Zend\Session\ManagerInterface as SessionManager;

class AuthenticationStorage extends Session
{
    /**
     * @param mixed $namespace
     * @param mixed $member
     * @param SessionManager $manager
     */
    public function __construct($namespace = null, $member = null, SessionManager $manager = null)
    {
        if ($this->sessionExists()) {
            parent::__construct($namespace, $member, $manager);
        } else {
            if ($namespace !== null) {
                $this->namespace = $namespace;
            }
            if ($member !== null) {
                $this->member = $member;
            }
        }
    }

    /**
     * Defined by Zend\Authentication\Storage\StorageInterface
     *
     * @return bool
     */
    public function isEmpty()
    {
        if ($this->sessionExists()) {
            return parent::isEmpty();
        }

        return true;
    }

    /**
     * Defined by Zend\Authentication\Storage\StorageInterface
     *
     * @return mixed
     */
    public function read()
    {
        if ($this->sessionExists()) {
            return parent::read();
        }

        return null;
    }

    /**
     * Defined by Zend\Authentication\Storage\StorageInterface
     *
     * @param  mixed $contents
     * @return void
     */
    public function write($contents)
    {
        if (!$this->sessionExists()) {
            parent::__construct($this->namespace, $this->member);
        }

        parent::write($contents);
    }

    /**
     * Defined by Zend\Authentication\Storage\StorageInterface
     *
     * @return void
     */
    public function clear()
    {
        if ($this->sessionExists()) {
            parent::clear();
        }
    }

    protected function sessionExists()
    {
        if (isset($_COOKIE[session_name()]) || (defined('SID') && session_id())) {
            return true;
        }

        return false;
    }
}
