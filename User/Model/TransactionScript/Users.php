<?php

namespace ZFS\User\Model\TransactionScript;

use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Db\Sql\Predicate\Like;
use ZFS\User\Model\Gateway\Traits\GatewayTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ZFS\User\Model\Gateway\AbstractFactory;
use Zend\Db\Sql\Expression;

class Users implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait, GatewayTrait;

    public function getAll()
    {
        $sql = $this->getUserGateway()->getSql();

        $statement = $sql->prepareStatementForSqlObject($this->select());

        return $statement->execute();
    }

    /**
     * @param string $order
     * @param string $orderDirection
     * @param string $filterColumn
     * @param string $filter
     *
     * @return \Zend\Db\Sql\Select
     */
    public function select($order = null, $orderDirection = null, $filterColumn = null, $filter = null)
    {
        $sql = $this->getUserGateway()->getSql();

        $select = $sql->select()
            ->columns(array(
                'id',
                'email',
                'name',
                'created',
                'updated',
                'role' => new Expression('GROUP_CONCAT(DISTINCT roles.name SEPARATOR ", ")'),
                'status',
            ))
            ->join(
                array('users_roles' => AbstractFactory::USERS_ROLES_TABLE),
                'users_roles.user_id = '.$this->getUserGateway()->table.'.id',
                array(),
                'Left'
            )
            ->join(
                array('roles' => AbstractFactory::ROLES_TABLE),
                'users_roles.role_id = roles.id',
                array(),
                'Left'
            )->group($this->getUserGateway()->table.'.id');

        if ($order) {
            $orderBy = $order;
            $orderDirection = $orderDirection ? : 'ASC';
            $select->order($orderBy . ' ' . $orderDirection);
        }

        if ($filterColumn && $filter) {
            $select->where(new Like($this->getUserGateway()->table.'.'.$filterColumn, '%'.$filter.'%'));
        }

        return $select;

    }

}
