<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 26.01.15
 * Time: 11:42
 */

namespace ZFS\User\Model\Object;

/**
 * @property int    id
 * @property int    userId
 * @property string provider
 * @property string foreignKey
 * @property string created
 * @property string updated
 * @property string expired
 * @property string status
 */
class Auth extends Base
{
    /**
     * Pending email verification
     */
    const STATUS_PENDING  = 'pending';
    /**
     * Active user
     */
    const STATUS_ACTIVE   = 'active';
    /**
     * Disabled by administrator
     */
    const STATUS_DISABLED = 'disabled';
    /**
     * Removed account
     */
    const STATUS_DELETED  = 'deleted';

    const PROVIDER_COOKIE   = 'cookie';
    const PROVIDER_EQUALS   = 'equals';
    const PROVIDER_FACEBOOK = 'facebook';
    const PROVIDER_GOOGLE   = 'google';
    const PROVIDER_TWITTER  = 'twitter';

    const ZFS_AUTH_NAMESPACE = 'zfs_auth';

    protected $primaryColumns = array(
        'id'
    );

    protected $fieldToColumnMap = array(
        'userId' => 'user_id',
        'foreignKey' => 'foreign_key',
    );
}
