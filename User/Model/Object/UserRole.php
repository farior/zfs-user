<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.01.15
 * Time: 14:57
 */

namespace ZFS\User\Model\Object;

/**
 * @property int userId
 * @property int roleId
 */
class UserRole extends Base
{
    protected $primaryColumns = array(
        'user_id', 'role_id'
    );

    protected $fieldToColumnMap = array(
        'userId' => 'user_id',
        'roleId' => 'role_id',
    );
}
