<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.01.15
 * Time: 13:26
 */

namespace ZFS\User\Model\Object;

use ZFS\DomainModel\Object\ObjectMagic;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class Base extends ObjectMagic implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
}
