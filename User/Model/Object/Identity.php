<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 03.02.15
 * Time: 12:07
 */

namespace ZFS\User\Model\Object;

/**
 * @property int    id
 * @property int    userId
 * @property string provider
 * @property string foreignKey
 * @property string created
 * @property string updated
 * @property string avatar
 * @property string status
 * @property string authStatus
 * @property string name
 * @property string email
 */
class Identity extends Base
{
}
