<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 30.01.15
 * Time: 16:04
 */

namespace ZFS\User\Model\Object;

/**
 * @property int    authId
 * @property string credential
 * @property string expired
 */
class AuthCredential extends Base
{
    protected $fieldToColumnMap = array(
        'authId' => 'auth_id'
    );
}
