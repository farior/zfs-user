<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 16:41
 */

namespace ZFS\User\Model\Object;

/**
 * @property int    id
 * @property string email
 * @property string name
 * @property string avatar
 * @property string created
 * @property string updated
 * @property string status
 */
class User extends Base
{
    protected $primaryColumns = array(
        'id'
    );
}
