<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.01.15
 * Time: 15:14
 */

namespace ZFS\User\Model\Object;

/**
 * @property int    id
 * @property string name
 * @property string created
 */
class Role extends Base
{
    const ROLE_GUEST = 'guest';
    const ROLE_USER  = 'user';
    const ROLE_ADMIN = 'admin';

    protected $primaryColumns = array(
        'id'
    );
}
