<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 30.01.15
 * Time: 19:37
 */

namespace ZFS\User\Model\Object;

/**
 * @property int    authId
 * @property string code
 * @property string action
 * @property string data
 * @property string created
 * @property string expired
 */
class AuthActivation extends Base
{
    const ACTION_ACTIVATION   = 'activation';
    const ACTION_CHANGE_EMAIL = 'email';
    const ACTION_RECOVERY     = 'recovery';
    const ACTION_REMOVE       = 'remove';
    const ACTION_CONFIRM      = 'confirm';
    const ACTION_CANCEL       = 'cancel';
    const ACTION_OLD_EMAIL_CONFIRM = 'old_email_confirm';
    const ACTION_NEW_EMAIL_CONFIRM = 'new_email_confirm';


    protected $primaryColumns = array(
        'auth_id'
    );

    protected $fieldToColumnMap = array(
        'authId' => 'auth_id'
    );
}
