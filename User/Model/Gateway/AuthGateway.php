<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 29.01.15
 * Time: 18:31
 */

namespace ZFS\User\Model\Gateway;

use ZFS\User\Model\Object\AuthCredential as AuthCredentialObject;
use ZFS\User\Model\Object\Identity as IdentityObject;
use ZFS\Common\Model\Gateway\BaseGateway;

class AuthGateway extends BaseGateway
{
    /**
     * @param array $where
     * @return IdentityObject
     */
    public function getIdentity($where)
    {
        $sql = $this->getSql();

        $select = $sql->select()
            ->columns(array(
                'id',
                'userId' => 'user_id',
                'provider',
                'foreignKey' => 'foreign_key',
                'created',
                'updated',
                'authStatus' => 'status',
            ))
            ->join(
                array('user' => AbstractFactory::USERS_TABLE),
                'user.id = '.$this->table.'.user_id',
                array('name', 'email', 'status', 'avatar'),
                'Left'
            )
            ->where($where)
            ->limit(1);

        $statement = $sql->prepareStatementForSqlObject($select);

        $result = $statement->execute()->current();

        if (!$result) {
            return null;
        }

        $identityObject = new IdentityObject($result);
        $identityObject->setServiceLocator($this->getServiceLocator());

        return $identityObject;
    }

    /**
     * @param int $auth_id
     * @param string $credential
     * @param string $expired
     */
    public function setCredential($auth_id, $credential, $expired = '0000-00-00 00:00:00')
    {
        /** @var $authCredentialGateway \ZFS\DomainModel\Gateway\TableGateway */
        $authCredentialGateway = $this->getServiceLocator()->get('AuthCredentialsGateway');

        $authCredentialGateway->delete(array('auth_id' => $auth_id));

        $credentialObject = new AuthCredentialObject();
        $credentialObject->authId     = $auth_id;
        $credentialObject->credential = $credential;
        $credentialObject->expired    = $expired;

        $authCredentialGateway->insertObject($credentialObject);
    }
}
