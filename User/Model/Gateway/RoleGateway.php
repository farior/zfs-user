<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.01.15
 * Time: 15:14
 */

namespace ZFS\User\Model\Gateway;

use Zend\Db\Sql\Expression;
use ZFS\Common\Model\Gateway\BaseGateway;

class RoleGateway extends BaseGateway
{
    /**
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getRoles()
    {
        $sql = $this->getSql();

        $select = $sql->select()
            ->columns(array(
                'name',
                'children' => new Expression('GROUP_CONCAT(DISTINCT child_name.name)'),
                'privileges' => new Expression('GROUP_CONCAT(DISTINCT privileges.privilege)'),
            ))
            ->join(
                array('parent' => AbstractFactory::ROLES_INHERITANCE_TABLE),
                'parent.parent_role = '.$this->table.'.id',
                array(),
                'Left'
            )
            ->join(
                array('privileges' => AbstractFactory::PRIVILEGES_TABLE),
                'privileges.role_id = '.$this->table.'.id',
                array(),
                'Left'
            )
            ->join(
                array('child_name' => $this->table),
                'child_name.id = parent.child_role',
                array(),
                'Left'
            )
            ->group($this->table.'.id');

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    /**
     * @param string $name
     * @return array|\ArrayObject|null
     */
    public function getRole($name)
    {
        return $this->selectOne(array('name' => $name));
    }
}
