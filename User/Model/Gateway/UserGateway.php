<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.01.15
 * Time: 14:56
 */

namespace ZFS\User\Model\Gateway;

use ZFS\User\Model\Db\Sql\InsertIgnore;
use Zend\Db\Sql\Expression;
use ZFS\Common\Model\Gateway\BaseGateway;

class UserGateway extends BaseGateway
{
    /**
     * @param int $userId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getUserRoles($userId)
    {
        $sql = $this->getSql();

        $select = $sql->select()
            ->columns(array())
            ->join(
                array('users_roles' => AbstractFactory::USERS_ROLES_TABLE),
                'users_roles.user_id = '.$this->table.'.id',
                array(),
                'Left'
            )
            ->join(
                array('roles' => AbstractFactory::ROLES_TABLE),
                'users_roles.role_id = roles.id',
                array('role_name' => 'name'),
                'Left'
            )
            ->where(array($this->table.'.id' => $userId));

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    /**
     * @param int $userId
     * @param array $roles
     */
    public function setUserRoles($userId, $roles)
    {
        $insert = new InsertIgnore(AbstractFactory::USERS_ROLES_TABLE);

        /** @var $roleGateway \ZFS\User\Model\Gateway\RoleGateway */
        $roleGateway = $this->getServiceLocator()->get('RoleGateway');

        $this->getServiceLocator()->get('UserRoleGateway')->removeUserRoles($userId);

        foreach ($roles as $role) {
            $select = $insert->values(array(
                    'user_id' => $userId,
                    'role_id' => $roleGateway->getRole($role)->id
                ));

            $this->getSql()->prepareStatementForSqlObject($select)->execute();
        }
    }
}
