<?php

namespace ZFS\User\Model\Gateway\Traits;

use ZFS\User\Model\Gateway\RoleGateway;
use ZFS\User\Model\Gateway\UserGateway;
use ZFS\User\Model\Gateway\AuthGateway;
use ZFS\User\Model\Gateway\UserRoleGateway;
use ZFS\Common\Model\Gateway\BaseGateway;

trait GatewayTrait
{
    /**
     * @var RoleGateway
     */
    protected $roleGateway = null;

    /**
     * @var UserGateway
     */
    protected $userGateway = null;

    /**
     * @var AuthGateway
     */
    protected $authGateway = null;

    /**
     * @var UserRoleGateway
     */
    protected $userRoleGateway = null;

    /**
     * @var BaseGateway
     */
    protected $privilegesGateway = null;

    /**
     * @var BaseGateway
     */
    protected $rolesInheritanceGateway = null;

    /**
     * @var BaseGateway
     */
    protected $authCredentialsGateway = null;

    /**
     * @var BaseGateway
     */
    protected $authActivationGateway = null;



    /**
     * Get role gateway
     *
     * @return RoleGateway
     */
    public function getRoleGateway()
    {
        if ($this->roleGateway == null) {
            $this->roleGateway = $this->getServiceLocator()->get('RoleGateway');
        }

        return $this->roleGateway;
    }

    /**
     * Get user gateway
     *
     * @return UserGateway
     */
    public function getUserGateway()
    {
        if ($this->userGateway == null) {
            $this->userGateway = $this->getServiceLocator()->get('UserGateway');
        }

        return $this->userGateway;
    }

    /**
     * Get auth gateway
     *
     * @return AuthGateway
     */
    public function getAuthGateway()
    {
        if ($this->authGateway == null) {
            $this->authGateway = $this->getServiceLocator()->get('AuthGateway');
        }

        return $this->authGateway;
    }

    /**
     * Get user_role gateway
     *
     * @return UserRoleGateway
     */
    public function getUserRoleGateway()
    {
        if ($this->userRoleGateway == null) {
            $this->userRoleGateway = $this->getServiceLocator()->get('UserRoleGateway');
        }

        return $this->userRoleGateway;
    }

    /**
     * Get privileges gateway
     *
     * @return BaseGateway
     */
    public function getPrivilegesGateway()
    {
        if ($this->privilegesGateway == null) {
            $this->privilegesGateway = $this->getServiceLocator()->get('PrivilegesGateway');
        }

        return $this->privilegesGateway;
    }

    /**
     * Get roles_inheritance gateway
     *
     * @return BaseGateway
     */
    public function getRolesInheritanceGateway()
    {
        if ($this->rolesInheritanceGateway == null) {
            $this->rolesInheritanceGateway = $this->getServiceLocator()->get('RolesInheritanceGateway');
        }

        return $this->rolesInheritanceGateway;
    }

    /**
     * Get auth_credentials gateway
     *
     * @return BaseGateway
     */
    public function getAuthCredentialsGateway()
    {
        if ($this->authCredentialsGateway == null) {
            $this->authCredentialsGateway = $this->getServiceLocator()->get('AuthCredentialsGateway');
        }

        return $this->authCredentialsGateway;
    }

    /**
     * Get auth_activation gateway
     *
     * @return BaseGateway
     */
    public function getAuthActivationGateway()
    {
        if ($this->authActivationGateway == null) {
            $this->authActivationGateway = $this->getServiceLocator()->get('AuthActivationGateway');
        }

        return $this->authActivationGateway;
    }
}
