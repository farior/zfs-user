<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.01.15
 * Time: 15:17
 */

namespace ZFS\User\Model\Gateway;

use ZFS\Common\Model\Gateway\BaseGateway;

class UserRoleGateway extends BaseGateway
{
    /**
     * @param int $userId
     * @param int $roleName
     */
    public function removeUserRole($userId, $roleName)
    {
        /** @var $roleGateway \ZFS\User\Model\Gateway\RoleGateway */
        $roleGateway = $this->getServiceLocator()->get('RoleGateway');

        $roleId = $roleGateway->getRole($roleName)->id;

        $this->delete(array(
            'user_id' => $userId,
            'role_id' => $roleId
        ));
    }

    /**
     * @param int $userId
     */
    public function removeUserRoles($userId)
    {
        $this->delete(array('user_id' => $userId));
    }
}
