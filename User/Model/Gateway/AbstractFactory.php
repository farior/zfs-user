<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 16:34
 */

namespace ZFS\User\Model\Gateway;

use ZFS\DomainModel\Gateway\AbstractFactory as BaseAbstractFactory;
use ZFS\DomainModel\Service\Options;

class AbstractFactory extends BaseAbstractFactory
{
    const USERS_TABLE             = 'zfs_users';
    const AUTH_TABLE              = 'zfs_auth';
    const AUTH_ACTIVATION_TABLE   = 'zfs_auth_activation';
    const AUTH_CREDENTIALS_TABLE  = 'zfs_auth_credentials';
    const ROLES_TABLE             = 'zfs_roles';
    const USERS_ROLES_TABLE       = 'zfs_users_roles';
    const PRIVILEGES_TABLE        = 'zfs_privileges';
    const ROLES_INHERITANCE_TABLE = 'zfs_roles_inheritance';

    protected $provides = array(
        'UserGateway' => array(
            Options::OPTION_TABLE_NAME       => self::USERS_TABLE,
            Options::OPTION_OBJECT_PROTOTYPE => 'ZFS\User\Model\Object\User',
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\User\Model\Gateway\UserGateway'
        ),
        'AuthGateway' => array(
            Options::OPTION_TABLE_NAME       => self::AUTH_TABLE,
            Options::OPTION_OBJECT_PROTOTYPE => 'ZFS\User\Model\Object\Auth',
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\User\Model\Gateway\AuthGateway'
        ),
        'RoleGateway' => array(
            Options::OPTION_TABLE_NAME       => self::ROLES_TABLE,
            Options::OPTION_OBJECT_PROTOTYPE => 'ZFS\User\Model\Object\Role',
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\User\Model\Gateway\RoleGateway'
        ),
        'UserRoleGateway' => array(
            Options::OPTION_TABLE_NAME       => self::USERS_ROLES_TABLE,
            Options::OPTION_OBJECT_PROTOTYPE => 'ZFS\User\Model\Object\UserRole',
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\User\Model\Gateway\UserRoleGateway'
        ),
        'PrivilegesGateway' => array(
            Options::OPTION_TABLE_NAME       => self::PRIVILEGES_TABLE,
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\Common\Model\Gateway\BaseGateway'
        ),
        'RolesInheritanceGateway' => array(
            Options::OPTION_TABLE_NAME       => self::ROLES_INHERITANCE_TABLE,
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\Common\Model\Gateway\BaseGateway'
        ),
        'AuthCredentialsGateway' => array(
            Options::OPTION_TABLE_NAME       => self::AUTH_CREDENTIALS_TABLE,
            Options::OPTION_OBJECT_PROTOTYPE => 'ZFS\User\Model\Object\AuthCredential',
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\Common\Model\Gateway\BaseGateway'
        ),
        'AuthActivationGateway' => array(
            Options::OPTION_TABLE_NAME       => self::AUTH_ACTIVATION_TABLE,
            Options::OPTION_OBJECT_PROTOTYPE => 'ZFS\User\Model\Object\AuthActivation',
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\Common\Model\Gateway\BaseGateway'
        )
    );
}
