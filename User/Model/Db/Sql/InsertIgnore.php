<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.01.15
 * Time: 15:28
 */

namespace ZFS\User\Model\Db\Sql;

use Zend\Db\Sql\Insert;

class InsertIgnore extends Insert
{
    /**
     * @var array Specification array
     */
    protected $specifications = array(
        self::SPECIFICATION_INSERT => 'INSERT IGNORE INTO %1$s (%2$s) VALUES (%3$s)'
    );
}
