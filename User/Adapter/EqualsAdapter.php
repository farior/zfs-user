<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 12.02.15
 * Time: 17:01
 */

namespace ZFS\User\Adapter;

use Zend\Authentication\Result;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Model\Object\Identity;
use Zend\Crypt\Password\Bcrypt;

class EqualsAdapter extends AuthAdapter
{
    protected $email;

    protected $password;

    public function setCredentials($email, $password)
    {
        $this->email    = $email;
        $this->password = $password;
    }

    /**
     * @return Result
     */
    public function authenticate()
    {
        /** @var $authGateway \ZFS\User\Model\Gateway\AuthGateway */
        $authGateway = $this->getServiceLocator()->get('AuthGateway');

        /** @var $identity Identity */
        $identity = $authGateway->getIdentity(array(
            'foreign_key' => $this->email,
            'provider'    => Auth::PROVIDER_EQUALS
        ));

        if (!$identity) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, array('User not found'));
        }

        /** @var $authCredentialsGateway \ZFS\Common\Model\Gateway\BaseGateway */
        $authCredentialsGateway = $this->getServiceLocator()->get('AuthCredentialsGateway');

        /** @var $credential \ZFS\User\Model\Object\AuthCredential */
        $credential = $authCredentialsGateway->selectOne(array('auth_id' => $identity->id));

        if (!$credential) {
            return new Result(Result::FAILURE, null, array('Credentials missing'));
        }

        // check password
        $bCrypt = new Bcrypt();
        if (!$bCrypt->verify($this->password, $credential->credential)) {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array('Wrong password'));
        }

        return $this->checkIdentity($identity);
    }
}
