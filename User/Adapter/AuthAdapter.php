<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 12.02.15
 * Time: 15:22
 */

namespace ZFS\User\Adapter;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use ZFS\User\Model\Object\Auth;
use Zend\Authentication\Result;
use ZFS\User\Model\Object\Identity;

abstract class AuthAdapter implements AdapterInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    abstract public function authenticate();

    /**
     * @param Identity $identity
     * @return Result
     */
    protected function checkIdentity($identity)
    {
        switch ($identity->status)
        {
            case Auth::STATUS_DELETED:

                return new Result(Result::FAILURE, null, array('Your account is deleted from system. Check your Email'));

            case Auth::STATUS_DISABLED:

                return new Result(Result::FAILURE, null, array('Your account is disabled by administrator'));
        }

        switch ($identity->authStatus)
        {
            case Auth::STATUS_PENDING:

                return new Result(Result::FAILURE, null, array('Your account is pending activation. Check your Email'));

            case Auth::STATUS_ACTIVE:

                return new Result(Result::SUCCESS, $identity);

            default:

                return new Result(Result::FAILURE, null, array('User status is undefined in system'));
        }
    }
}
