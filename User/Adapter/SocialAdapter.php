<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 12.02.15
 * Time: 17:02
 */

namespace ZFS\User\Adapter;

use Zend\Authentication\Result;
use ZFS\User\Model\Object\Identity;
use ZFS\User\Social\Container\OpauthSocialContainer;

class SocialAdapter extends AuthAdapter
{
    /** @var $socialData OpauthSocialContainer */
    protected $socialData;

    /**
     * @param OpauthSocialContainer $socialData
     */
    public function setSocialData(OpauthSocialContainer $socialData)
    {
        $this->socialData = $socialData;
    }

    /**
     * @return Result
     */
    public function authenticate()
    {
        /** @var $authGateway \ZFS\User\Model\Gateway\AuthGateway */
        $authGateway = $this->getServiceLocator()->get('AuthGateway');

        /** @var $identity Identity */
        $identity = $authGateway->getIdentity(array(
            'foreign_key' => $this->socialData->uid,
            'provider'    => $this->socialData->provider
        ));

        if (!$identity) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null);
        }

        return $this->checkIdentity($identity);
    }
}
