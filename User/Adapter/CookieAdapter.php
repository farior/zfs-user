<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 12.02.15
 * Time: 17:02
 */

namespace ZFS\User\Adapter;

use Zend\Authentication\Result;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Model\Object\Identity;

class CookieAdapter extends AuthAdapter
{
    protected $rId;

    protected $rToken;

    public function setCookie($rId, $rToken)
    {
        $this->rId    = $rId;
        $this->rToken = $rToken;
    }

    /**
     * @return Result
     */
    public function authenticate()
    {
        /** @var $authGateway \ZFS\User\Model\Gateway\AuthGateway */
        $authGateway = $this->getServiceLocator()->get('AuthGateway');

        /** @var $identity Identity */
        $identity = $authGateway->getIdentity(array(
            'user_id' => $this->rId,
            'provider' => Auth::PROVIDER_COOKIE
        ));

        if (!$identity) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, array('User not found'));
        }

        /** @var $authCredentialsGateway \ZFS\Common\Model\Gateway\BaseGateway */
        $authCredentialsGateway = $this->getServiceLocator()->get('AuthCredentialsGateway');

        /** @var $credential \ZFS\User\Model\Object\AuthCredential */
        $credential = $authCredentialsGateway->selectOne(array('auth_id' => $identity->id));

        if (!$credential) {
            return new Result(Result::FAILURE, null, array('Credentials missing'));
        }

        $datetime1 = new \DateTime(); // now
        $datetime2 = new \DateTime($credential->expired);
        $interval  = $datetime1->diff($datetime2);

        if (($this->rToken == $credential->credential) && (!$interval->invert)) {
            return $this->checkIdentity($identity);
        }

        return new Result(Result::FAILURE, null);
    }
}
