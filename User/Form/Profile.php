<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 12.03.15
 * Time: 17:59
 */

namespace ZFS\User\Form;

use Zend\Form\Form;

class Profile extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('zfsprofile');

        $this->add(array(
            'type' => 'Email',
            'name' => 'email',
            'attributes' => array(
                'class' => 'form-control',
                'title' => 'Email must be in valid format. Example: JSmith@example.com',
                'maxlength' => '255',
                'required' => true
            )
        ));

        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'type' => 'Text',
            'name' => 'username',
            'attributes' => array(
                'class' => 'form-control',
                'maxlength' => '255'
            )
        ));

        $this->add(array(
            'type' => 'File',
            'name' => 'avatar',
            'attributes' => array(
                'class' => 'text-center center-block well well-sm',
            )
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'attributes' => array(
                'class' => 'form-control',
                'title' => 'Password must be less than or equal to 255 characters',
                'maxlength' => '255'
            )
        ));

        $this->add(array(
            'name' => 'password2',
            'type' => 'Password',
            'attributes' => array(
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save Changes',
                'class' => 'btn btn-primary'
            )
        ));
    }

    public function getInputFilter()
    {
        if (!$this->filter) {
            parent::getInputFilter();

            $this->filter->add(array(
                'name'     => 'email',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'Email is required'
                            )
                        )
                    ),
                    array(
                        'name' => 'EmailAddress',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 5,
                            'max' => 255,
                            'messages' => array(
                                'emailAddressInvalidFormat' => 'Email address format is invalid'
                            )
                        )
                    )
                )
            ));

            $this->filter->add(array(
                'name'     => 'username',
                'filters'  => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'max' => 255,
                            'messages' => array(
                                'stringLengthTooLong' => 'Password must be no more than 255 characters',
                            )
                        )
                    ),
                    array(
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/^[a-z0-9 &-_\.,@]{0,255}$/i',
                            'messages' => array(
                                \Zend\Validator\Regex::INVALID => 'Invalid input, only a-z, 0-9 & - _ . characters allowed',
                            ),
                        ),
                    ),
                )
            ));

            $this->filter->add(array(
                'name'     => 'avatar',
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\File\Size',
                        'options' => array(
                            'min' => 120,
                            'max' => 2097152,
                        ),
                    ),
                    array(
                        'name' => 'Zend\Validator\File\Extension',
                        'options' => array(
                            'extension' => 'gif,jpg,jpeg,png',
                        ),
                    ),
                ),
            ));

            $this->filter->add(array(
                'name'     => 'password',
                'continue_if_empty' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'max' => 255,
                            'messages' => array(
                                'stringLengthTooLong' => 'Password must be no more than 255 characters',
                            )
                        )
                    )
                )
            ));

            $this->filter->add(array(
                'name'     => 'password2',
                'continue_if_empty' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'Identical',
                        'options' => array(
                            'token' => 'password',
                            'messages' => array(
                                'notSame' => 'Password does not match'
                            )
                        )
                    )
                )
            ));
        }

        return $this->filter;
    }
}
