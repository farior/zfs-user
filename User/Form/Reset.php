<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.02.15
 * Time: 17:19
 */

namespace ZFS\User\Form;

use Zend\Form\Form;

class Reset extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('reset');

        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'enter new password',
                'title' => 'Password must be at least 5 characters, but less than or equal to 255 characters',
                'maxlength' => '255',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 'password2',
            'type' => 'Password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'repeat new password',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Change',
                'class' => 'btn btn-primary'
            )
        ));
    }

    public function getInputFilter()
    {
        if (!$this->filter) {
            parent::getInputFilter();

            $this->filter->add(array(
                'name'     => 'password',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'Password is required.'
                            )
                        )
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'min' => 5,
                            'max' => 255,
                            'messages' => array(
                                'stringLengthTooLong' => 'Password must be no more than 255 characters',
                                'stringLengthTooShort' => 'Password must be more than 5 characters'
                            )
                        )
                    )
                )
            ));

            $this->filter->add(array(
                'name'     => 'password2',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'Identical',
                        'options' => array(
                            'token' => 'password',
                            'messages' => array(
                                'notSame' => 'Password does not match'
                            )
                        )
                    )
                )
            ));
        }

        return $this->filter;
    }
}
