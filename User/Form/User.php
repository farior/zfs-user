<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.01.15
 * Time: 16:47
 */

namespace ZFS\User\Form;

class User extends Base
{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->get('submit')->setValue('Continue');

        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'password',
                'title' => 'Password must be at least 5 characters, but less than or equal to 255 characters',
                'maxlength' => '255',
                'required' => true
            )
        ));
    }

    public function getInputFilter()
    {
        if (!$this->filter) {
            parent::getInputFilter();

            $this->filter->add(array(
                'name'     => 'password',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'Password is required.'
                            )
                        )
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'min' => 5,
                            'max' => 255,
                            'messages' => array(
                                'stringLengthTooLong' => 'Password must be no more than 255 characters',
                                'stringLengthTooShort' => 'Password must be more than 5 characters'
                            )
                        )
                    )
                )
            ));
        }

        return $this->filter;
    }
}
