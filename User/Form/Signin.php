<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 19:14
 */

namespace ZFS\User\Form;

class Signin extends User
{
    public function __construct($name = null)
    {
        parent::__construct('zfslogin');

        $this->get('submit')->setValue('Sign in');

        $this->add(array(
            'name' => 'rememberMe',
            'type' => 'Checkbox',
            'options' => array(
                'label' => 'Remember me',
                'use_hidden_element' => true
            ),
            'attributes' => array(
                'checked' => true
            )
        ));
    }
}
