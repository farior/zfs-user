<?php
/**
 * User: dev
 * Date: 23.02.15 19:15
 */

namespace ZFS\User\Form\Management;

use Zend\Form\Form;
use ZFS\User\Model\Object\Auth as AuthObject;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use ZFS\DomainModel\ResultSet\ResultSet;

class User extends Form implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden'
        ));

        $this->add(array(
            'type' => 'Email',
            'name' => 'email',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'email',
                'title' => 'Email must be in valid format. Example: JSmith@example.com',
                'maxlength' => '255',
                'required' => true
            )
        ));

        $this->add(array(
            'type' => 'Text',
            'name' => 'name',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'name',
                'maxlength' => '255'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'password',
                'title' => 'Password must be at least 5 characters, but less than or equal to 255 characters',
                'maxlength' => '255',
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    AuthObject::STATUS_ACTIVE => AuthObject::STATUS_ACTIVE,
                    AuthObject::STATUS_DELETED => AuthObject::STATUS_DELETED,
                    AuthObject::STATUS_DISABLED => AuthObject::STATUS_DISABLED
                )
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Status',
                'maxlength' => '255',
                'value' => AuthObject::STATUS_ACTIVE,
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'class' => 'btn btn-primary'
            )
        ));
    }

    public function setRoles(ResultSet $roles)
    {
        $roles = array_values(iterator_to_array($roles));

        $options = array();

        foreach ($roles as $role) {
            $options[$role->name] = $role->name;
        }

        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'roles',
            'options' => array(
                'label' => 'Roles',
                'value_options' => $options
            ),
            'attributes' => array(
                'value' => $roles[0]->name,
            )
        ));
    }

    public function getInputFilter()
    {
        if (!$this->filter) {
            parent::getInputFilter();
        }

        return $this->filter;
    }
}
