<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 19:17
 */

namespace ZFS\User\Form;

class Signup extends User
{
    public function __construct($name = null)
    {
        parent::__construct('zfssignup');

        $this->get('submit')->setValue('Registration');

        $this->add(array(
            'type' => 'Text',
            'name' => 'username',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'name',
                'maxlength' => '255'
            )
        ));

        $this->add(array(
            'name' => 'password2',
            'type' => 'Password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'repeat password',
                'required' => true
            )
        ));


    }

    public function getInputFilter()
    {
        if (!$this->filter) {
            parent::getInputFilter();

            $this->filter->add(array(
                'name'     => 'username',
                'filters'  => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'max' => 255,
                            'messages' => array(
                                'stringLengthTooLong' => 'Password must be no more than 255 characters',
                            )
                        )
                    ),
                    array(
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/^[a-z0-9 &-_\.,@]{0,255}$/i',
                        ),
                    ),
                )
            ));

            $this->filter->add(array(
                'name'     => 'password2',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'Identical',
                        'options' => array(
                            'token' => 'password',
                            'messages' => array(
                                'notSame' => 'Password does not match'
                            )
                        )
                    )
                )
            ));
        }

        return $this->filter;
    }
}
