<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 18:06
 */

namespace ZFS\User\Form;

use Zend\Form\Form;

class Base extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden'
        ));

        $this->add(array(
            'type' => 'Email',
            'name' => 'email',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'email',
                'title' => 'Email must be in valid format. Example: JSmith@example.com',
                'maxlength' => '255',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Continue',
                'class' => 'btn btn-primary'
            )
        ));
    }

    public function getInputFilter()
    {
        if (!$this->filter) {
            parent::getInputFilter();

            $this->filter->add(array(
                'name'     => 'email',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'Email is required'
                            )
                        )
                    ),
                    array(
                        'name' => 'EmailAddress',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 5,
                            'max' => 255,
                            'messages' => array(
                                'emailAddressInvalidFormat' => 'Email address format is invalid'
                            )
                        )
                    )
                )
            ));
        }

        return $this->filter;
    }
}
