<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.04.15
 * Time: 13:01
 */

namespace ZFS\User\Services;

use Zend\Authentication\Result;
use Zend\Http\Response;
use Zend\Crypt\Password\Bcrypt;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Model\Object\AuthActivation;
use ZFS\User\Model\Object\User as UserObject;
use ZFS\User\Model\Object\Auth as AuthObject;
use ZFS\User\Model\Object\Role as RoleObject;
use Zend\Http\Header\SetCookie;
use ZFS\User\Model\Object\Identity;
use ZFS\User\Model\Gateway\Traits\GatewayTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AuthService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait, GatewayTrait;

    /**
     * Time that the token remains valid
     *
     * remember me for 31 day 3600 * 24 * 31
     */
    const COOKIE_EXPIRATION_TIME = 2678400;


    /**
     * Generate cookie for current identity
     */
    public function generateCookie()
    {
        /** @var $authService \Zend\Authentication\AuthenticationService */
        $authService = $this->getServiceLocator()->get('ZFS\AuthService');

        $identity = $authService->getIdentity();

        $this->getAuthGateway()->delete(array(
            'user_id'     => $identity->userId,
            'foreign_key' => $identity->email,
            'provider'    => AuthObject::PROVIDER_COOKIE
        ));

        $auth = new AuthObject();
        $auth->userId     = $identity->userId;
        $auth->foreignKey = $identity->email;
        $auth->provider   = AuthObject::PROVIDER_COOKIE;
        $auth->expired    = gmdate('Y-m-d H:i:s', time() + self::COOKIE_EXPIRATION_TIME);
        $auth->status     = AuthObject::STATUS_ACTIVE;

        $this->getAuthGateway()->insertObject($auth);

        $credential = $this->generateCode();

        $this->getAuthGateway()->setCredential($auth->id, $credential, $auth->expired);

        /** @var $response \Zend\Http\Response */
        $response = $this->getServiceLocator()->get('Response');

        /** @var $headers \Zend\Http\Headers */
        $headers = $response->getHeaders();

        $headers->addHeader(new SetCookie('rToken', $credential, time() + self::COOKIE_EXPIRATION_TIME, '/'));
        $headers->addHeader(new SetCookie('rId', $auth->userId, time() + self::COOKIE_EXPIRATION_TIME, '/'));
    }

    /**
     * @param UserObject $user
     * @param string $password
     * @param string $status
     * @return int
     */
    public function generateEquals($user, $password, $status = AuthObject::STATUS_PENDING)
    {
        $auth = $this->getAuthGateway()->selectOne(array(
            'user_id'     => $user->id,
            'foreign_key' => $user->email,
            'provider'    => AuthObject::PROVIDER_EQUALS
        ));

        if (!$auth) {
            $auth = new AuthObject();
            $auth->userId      = $user->id;
            $auth->foreignKey  = $user->email;
            $auth->provider    = AuthObject::PROVIDER_EQUALS;
            $auth->status      = $status;

            $this->getAuthGateway()->insertObject($auth);
        }

        // encrypt password and save as token
        $bCrypt = new Bcrypt();
        $credential = $bCrypt->create($password);

        $this->getAuthGateway()->setCredential($auth->id, $credential);

        return $auth->id;
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     * @return AuthObject
     */
    public function generateSocial($socialData)
    {
        /** @var $userExist UserObject */
        $userExist = $this->getUserGateway()->selectOne(array('email' => $socialData->email));

        $authObject = new AuthObject();

        $userRoles = $socialData->hasProvidedEmail() ? array(RoleObject::ROLE_USER) : array(RoleObject::ROLE_GUEST);

        /** @var $imageService \ZFS\User\Services\ImageService */
        $imageService = $this->getServiceLocator()->get('ZFS\Service\Image');

        if (!$userExist) {
            $user = new UserObject();
            $user->email  = $socialData->email;
            $user->name   = $socialData->name;
            $user->status = AuthObject::STATUS_ACTIVE;

            $this->getUserGateway()->saveObject($user);

            if ($socialData->image) {
                $imageService->saveAvatar($socialData->image, $user->id, true);
            }

            $this->getUserGateway()->setUserRoles($user->id, $userRoles);

            $authObject->userId = $user->id;
        } else {
            $authObject->userId = $userExist->id;

            if (!$userExist->avatar && $socialData->image) {
                $imageService->saveAvatar($socialData->image, $userExist->id, true);
            }
        }

        $authObject->foreignKey = $socialData->uid;
        $authObject->provider   = $socialData->provider;
        $authObject->status     = $socialData->hasProvidedEmail() ? Auth::STATUS_ACTIVE : Auth::STATUS_PENDING;

        $this->getAuthGateway()->insertObject($authObject);

        if ($authObject->status === AuthObject::STATUS_PENDING) {
            /** @var $mailService \ZFS\User\Services\MailService */
            $mailService = $this->getServiceLocator()->get('ZFS\Service\Mail');
            $mailService->sendActivation($authObject->id, AuthActivation::ACTION_CONFIRM);
        }

        return $authObject;
    }

    /**
     * @return Identity
     */
    public function updateIdentity()
    {
        /** @var $authService \Zend\Authentication\AuthenticationService */
        $authService = $this->getServiceLocator()->get('ZFS\AuthService');

        if (!$authService->hasIdentity()) {
            return null;
        }

        $identity = $this->getAuthGateway()->getIdentity(array(
            'user_id' => $authService->getIdentity()->userId,
            'provider' => $authService->getIdentity()->provider
        ));

        $authService->getStorage()->write($identity);

        return $identity;
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     */
    public function linkSocial($socialData)
    {
        /** @var $authService \Zend\Authentication\AuthenticationService */
        $authService = $this->getServiceLocator()->get('ZFS\AuthService');

        if ($authService->hasIdentity()) {
            $existIdentity = $this->checkSocial($socialData);

            if ($existIdentity) {
                $identity = $authService->getIdentity();

                $this->getAuthGateway()->update(
                    array('user_id' => $identity->userId),
                    array('id' => $existIdentity->id)
                );

//                $existAuth = $authGateway->select(array('user_id' => $existIdentity->userId));
//
//                if (!$existAuth->count()) {
//                    /** @var $userGateway \ZFS\User\Model\Gateway\UserGateway */
//                    $userGateway = $this->getController()->getServiceLocator()->get('UserGateway');
//
//                    $userGateway->delete(array('id' => $existIdentity->userId));
//                }

                return;
            }
        }

        $this->generateSocial($socialData);
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     * @return bool|Result
     */
    public function authenticateSocial($socialData)
    {
        /** @var $socialAdapter \ZFS\User\Adapter\SocialAdapter */
        $socialAdapter = $this->getServiceLocator()->get('ZFS\SocialAdapter');
        $socialAdapter->setSocialData($socialData);

        /** @var $authService \Zend\Authentication\AuthenticationService */
        $authService = $this->getServiceLocator()->get('ZFS\AuthService');
        $authService->setAdapter($socialAdapter);
        $authResult = $authService->authenticate();

        if (!$authService->hasIdentity()) {
            if (Result::FAILURE_IDENTITY_NOT_FOUND === $authResult->getCode()) {
                $authObject = $this->generateSocial($socialData);

                if ($authObject->status === Auth::STATUS_PENDING) {
                    return false;
                }

                $authResult = $authService->authenticate();
            }
        }

        return $authResult;
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     * @return Identity
     *
     * @codeCoverageIgnore
     */
    public function checkSocial($socialData)
    {
        /** @var $identity Identity */
        $identity = $this->getAuthGateway()->getIdentity(array(
            'foreign_key' => $socialData->uid,
            'provider'    => $socialData->provider
        ));

        return $identity;
    }

    /**
     * @codeCoverageIgnore
     */
    public function removeCookie()
    {
        /** @var $request \Zend\Http\Request */
        $request = $this->getServiceLocator()->get('Request');

        /** @var $cookie \Zend\Http\Header\Cookie */
        $cookie = $request->getCookie();

        if (isset($cookie->rToken) && isset($cookie->rId)) {
            $this->getAuthGateway()->delete(array(
                'user_id'     => $cookie->rId,
                'provider'    => AuthObject::PROVIDER_COOKIE
            ));

            /** @var $response \Zend\Http\Response */
            $response = $this->getServiceLocator()->get('Response');

            /** @var $headers \Zend\Http\Headers */
            $headers = $response->getHeaders();

            $headers->addHeader(new SetCookie('rToken', '', time() - self::COOKIE_EXPIRATION_TIME, '/'));
            $headers->addHeader(new SetCookie('rId', '', time() - self::COOKIE_EXPIRATION_TIME, '/'));
        }
    }

    public function generateCode()
    {
        $random = range('a', 'z', rand(1, 10));
        shuffle($random);
        return md5(join('', $random) . time());
    }
}
