<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.04.15
 * Time: 13:49
 */

namespace ZFS\User\Services;

use ZFS\User\Model\Gateway\Traits\GatewayTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use ZFS\User\Model\Gateway\AbstractFactory;
use ZFS\User\Model\Object\AuthActivation;
use ZFStarterMail\Model\Mail;

class MailService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait, GatewayTrait;

    /**
     * @param string $to
     * @param string $template
     * @param array  $params
     * @param string $name
     * @return bool
     *
     */
    public function sendMail($to, $template, $params = array(), $name = 'Name')
    {
        $data = array(
            'templateName' => $template,
            'toEmail' => $to,
            'toName' => $name,
            'params' => $params,
        );

        try {
            Mail::sendMail($this->getServiceLocator(), $data);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param int    $auth_id
     * @param string $action
     * @param string $data
     * @param string $toEmail
     * @param int    $expired
     * @return bool
     */
    public function sendActivation($auth_id, $action, $data = null, $toEmail = null, $expired = 5)
    {
        $identity = $this->getAuthGateway()->getIdentity(array(
            AbstractFactory::AUTH_TABLE.'.id' => $auth_id
        ));

        if (!$identity) {
            return false;
        }

        $this->getAuthActivationGateway()->delete(array(
            'auth_id' => $auth_id,
            'action'  => $action
        ));

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $this->getServiceLocator()->get('ZFS\Service\Auth');
        $code = $authService->generateCode();

        /** @var $urlPlugin \Zend\Mvc\Controller\Plugin\Url */
        $urlPlugin = $this->getServiceLocator()->get('controllerpluginmanager')->get('url');

        $activationUrl = $urlPlugin->fromRoute(
            'zfsuser/activation',
            array(),
            array(
                'force_canonical' => true,
                'query' => array(
                    'code' => $code,
                    'action' => $action
                )
            )
        );

        $cancelUrl = $urlPlugin->fromRoute(
            'zfsuser/activation',
            array(),
            array(
                'force_canonical' => true,
                'query' => array(
                    'code' => $code,
                    'action' => AuthActivation::ACTION_CANCEL
                )
            )
        );

        $toName = $identity->name ? : $identity->email;
        $toEmail = $toEmail ? : $identity->email;

        $alias = '';
        $params = array(
            'activationUrl' => $activationUrl,
            'cancelUrl' => $cancelUrl
        );

        switch ($action)
        {
            case AuthActivation::ACTION_ACTIVATION:
                $alias = 'activation_mail';
                break;

            case AuthActivation::ACTION_CONFIRM:
                $alias = 'confirm_mail';
                $params['social'] = $identity->provider;
                break;

            case AuthActivation::ACTION_RECOVERY:
                $alias = 'recovery_mail';
                $params['social'] = $identity->provider;
                break;

            case AuthActivation::ACTION_OLD_EMAIL_CONFIRM:
                $alias = 'old_mail_confirm';
                break;

            case AuthActivation::ACTION_NEW_EMAIL_CONFIRM:
                $alias = 'new_mail_confirm';
                break;
        }

        $mailSent = $this->sendMail($toEmail, $alias, $params, $toName);

        if ($mailSent) {
            $authActivation = new AuthActivation();
            $authActivation->authId  = $auth_id;
            $authActivation->action  = $action;
            $authActivation->code    = $code;
            $authActivation->expired = date('Y-m-d H:i:s', strtotime("+$expired day"));
            if ($data) {
                $authActivation->data = serialize($data);
            }

            $this->getAuthActivationGateway()->insertObject($authActivation);

            return true;
        }

        return false;
    }
}
