<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.04.15
 * Time: 13:40
 */

namespace ZFS\User\Services;

use ZFS\User\Model\Gateway\Traits\GatewayTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;

class ImageService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait, GatewayTrait;

    /**
     * @param string $image
     * @param int    $userId
     * @param bool   $rewrite
     */
    public function saveAvatar($image, $userId, $rewrite = false)
    {
        $config = $this->getServiceLocator()->get('Config');

        $imagine = new Imagine();

        $avatarPath = getcwd() . '/public/' . $config['avatar']['save_folder'];

        $avatarFileName = 'user_'.$userId.'.jpg';

        if (file_exists($avatarPath.'/'.$avatarFileName) && !$rewrite) {
            return;
        }

        if (!file_exists($avatarPath)) {
            mkdir($avatarPath);
            chmod($avatarPath, 0775);
        }

        $imagine->open($image)
            ->thumbnail(new Box(
                $config['avatar']['size']['width'],
                $config['avatar']['size']['height']
            ), ImageInterface::THUMBNAIL_INSET)
            ->save($avatarPath.'/'.$avatarFileName, array('jpeg_quality' => $config['avatar']['quality']));

        $this->getUserGateway()->update(
            array('avatar' => $config['avatar']['save_folder'] . '/' . $avatarFileName),
            array('id' => $userId)
        );

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $this->getServiceLocator()->get('ZFS\Service\Auth');
        $authService->updateIdentity();
    }
}
