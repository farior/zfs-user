<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.01.15
 * Time: 16:10
 */

namespace ZFS\User\Services\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZFS\User\Model\Object\Auth as AuthObject;
use ZFS\User\Session\AuthenticationStorage;

class SessionStorageFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new AuthenticationStorage(AuthObject::ZFS_AUTH_NAMESPACE);
    }
}
