<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.02.15
 * Time: 12:24
 */

namespace ZFS\User\Services\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZFS\User\Session\AuthenticationStorage;

class SocialStorageFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new AuthenticationStorage('zfs_social');
    }
}
