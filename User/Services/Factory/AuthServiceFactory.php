<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27.01.15
 * Time: 16:10
 */

namespace ZFS\User\Services\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService;

class AuthServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var $sessionStorage \Zend\Authentication\Storage\Session */
        $sessionStorage = $serviceLocator->get('ZFS\SessionStorage');

        return new AuthenticationService($sessionStorage);
    }
}
