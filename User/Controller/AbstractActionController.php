<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 15:20
 */

namespace ZFS\User\Controller;

use Zend\Mvc\Controller\AbstractActionController as ZendAbstractController;
use Zend\Mvc\MvcEvent;
use ZFS\User\Controller\Plugin\AuthPlugin;
use ZFS\User\Controller\Plugin\ImagePlugin;
use ZFS\User\Controller\Plugin\MailPlugin;
use ZFS\Common\Controller\Plugin\FlashMessenger;
use ZFS\User\Model\Gateway\Traits\GatewayTrait;

/**
 * @property object $routes
 *
 * @method AuthPlugin AuthPlugin()
 * @method ImagePlugin ImagePlugin()
 * @method MailPlugin MailPlugin()
 * @method FlashMessenger flashMessenger()
 *
 */
abstract class AbstractActionController extends ZendAbstractController
{
    use GatewayTrait;

    public function onDispatch(MvcEvent $e)
    {
        $this->routes = (object)$this->getServiceLocator()->get('Config')['default_routes'];

        return parent::onDispatch($e);
    }
}
