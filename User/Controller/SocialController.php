<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.03.15
 * Time: 11:59
 */

namespace ZFS\User\Controller;

use Zend\Http\Request;

class SocialController extends AbstractActionController
{
    public function socialRemoveAction()
    {
        $authId = (int)$this->params()->fromRoute('id');

        if ($authId) {
            $this->getAuthGateway()->delete(array('id' => $authId));
        }

        return $this->redirect()->toRoute($this->routes->login_redirect);
    }

    /**
     * @codeCoverageIgnore
     */
    public function socialLoginAction()
    {
        $provider = $this->params()->fromRoute('provider');
        $config   = $this->getServiceLocator()->get('Config')['opauth'];

        $config   = array_merge($config, array(
            'path'         => $this->url()->fromRoute('zfsuser/socialLogin'),
            'callback_url' => $this->url()->fromRoute('zfsuser/socialCallback', array('provider' => $provider))
        ));

        new \Opauth($config);

        return false;
    }

    public function socialCallbackAction()
    {
        $request = $this->getRequest();

        if (!$request instanceof Request) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        if (!$request->isPost() || (!isset($request->getPost()['opauth']))) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        /** @var $socialData \ZFS\User\Social\Container\SocialContainerInterface */
        $socialData = $this->getServiceLocator()->get('SocialContainer')->get($request->getPost());

        if ($socialData->hasError()) {
            $this->flashMessenger()->addWarningMessage($socialData->getErrorMessage());

            return $this->redirect()->toRoute($this->routes->social_login_error);
        }

        /** @var $authService \Zend\Authentication\AuthenticationService */
        $authService = $this->getServiceLocator()->get('ZFS\AuthService');
        if ($authService->hasIdentity()) {
            /** @var $identity \ZFS\User\Model\Object\Identity */
            $identity = $authService->getIdentity();

            $socialData->email = $identity->email;
            $socialData->setProvidedEmail();
        }

        // Let's choose a strategy, depending on whether social provide email or not

        /** @var $strategy \ZFS\User\Social\SocialStrategy */
        $strategy = $this->getServiceLocator()->get('ZFS\SocialLogin')->getStrategy($request->getPost());

        return $strategy->preProcess($socialData);
    }

    public function socialConfirmAction()
    {
        /** @var $socialStorage \Zend\Authentication\Storage\Session */
        $socialStorage = $this->getServiceLocator()->get('ZFS\SocialStorage');

        $request = $this->getRequest();

        if ((!$request instanceof Request) || $socialStorage->isEmpty()) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        /** @var $strategy \ZFS\User\Social\SocialStrategy */
        $strategy = $this->getServiceLocator()->get('ZFS\SocialLogin')->getStrategy($request->getPost());

        $viewModel = $strategy->postProcess($request->getPost());

        if ($socialStorage->isEmpty()) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        return $viewModel;
    }
}
