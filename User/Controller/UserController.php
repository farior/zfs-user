<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 15:13
 */

namespace ZFS\User\Controller;

use Zend\Authentication\Result;
use Zend\View\Model\ViewModel;
use ZFS\User\Form\Profile as ProfileForm;
use ZFS\User\Form\Signup as SignupForm;
use ZFS\User\Form\Signin as SigninForm;
use ZFS\User\Model\Object\AuthActivation;
use ZFS\User\Model\Object\User as UserObject;
use ZFS\User\Model\Object\Role as RoleObject;
use ZFS\User\Model\Object\Auth as AuthObject;
use Zend\Http\Request;

class UserController extends AbstractActionController
{
    public function indexAction()
    {
        /** @var $authenticationService \Zend\Authentication\AuthenticationService */
        $authenticationService = $this->getServiceLocator()->get('ZFS\AuthService');

        if (!$authenticationService->hasIdentity()) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        $config = $this->getServiceLocator()->get('Config');

        /** @var $identity \ZFS\User\Model\Object\Identity */
        $identity = $authenticationService->getIdentity();

        $profileForm = new ProfileForm();

        $request = $this->getRequest();

        if (($request instanceof Request) && $request->isPost()) {
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $profileForm->setData($post);

            // Update image
            if (!$post['avatar']['error']) {
                $this->ImagePlugin()->saveAvatar($post['avatar']['tmp_name'], $identity->userId, true);
            }

            if (!$profileForm->isValid()) {
                foreach ($profileForm->getInputFilter()->getMessages() as $messages) {
                    foreach ($messages as $message) {
                        $this->flashMessenger()->addWarningMessage($message);
                    }
                }
            } else {
                $data = $profileForm->getData();

                // Update password
                if ($data['password']) {
                    if (strlen($data['password']) >= 5) {
                        /** @var $authorizations \ZFS\User\Model\Object\User */
                        $user = $this->getUserGateway()->selectOne(array('id' => $identity->userId));

                        $this->AuthPlugin()->generateEquals($user, $data['password'], AuthObject::STATUS_ACTIVE);

                        $this->flashMessenger()->addSuccessMessage('password_changed', array('email' => $identity->email));
                    } else {
                        $this->flashMessenger()->addWarningMessage('password_short');
                    }
                }

                // Update username
                if ($data['username'] && strlen($data['username']) > 0 && $data['username'] != $identity->name) {
                    $this->getUserGateway()->update(
                        array('name' => $data['username']),
                        array('id' => $identity->userId)
                    );
                }

                // Change email
                if ($data['email'] && $data['email'] != $identity->email) {
                    $userExist = $this->getUserGateway()->selectOne(array('email' => $data['email']));

                    if ($userExist) {
                        $this->flashMessenger()->addWarningMessage('email_exist', array('email' => $data['email']));
                    } else {
                        $mailResult = $this->MailPlugin()->sendActivation(
                            $identity->id,
                            AuthActivation::ACTION_OLD_EMAIL_CONFIRM,
                            array(
                                'email' => $data['email']
                            )
                        );

                        if ($mailResult) {
                            $this->flashMessenger()->addSuccessMessage(
                                'old_email_confirm',
                                array('email' => $identity->email)
                            );
                        } else {
                            $this->flashMessenger()->addErrorMessage('mail_error');
                        }
                    }
                }

                /** @var $authService \ZFS\User\Services\AuthService */
                $authService = $this->getServiceLocator()->get('ZFS\Service\Auth');
                $authService->updateIdentity();
            }

            return $this->redirect()->toRoute($this->routes->login_redirect);
        }

        /** @var $authorizations \ZFS\User\Model\Object\Auth[] */
        $authorizations = $this->getAuthGateway()->select(array('user_id' => $identity->userId));

        $profileForm->setData(array(
            'username' => $identity->name,
            'email'    => $identity->email
        ));

        return new ViewModel(array(
            'authorizations' => $authorizations,
            'form'           => $profileForm,
            'config'         => $config
        ));
    }

    /**
     * @codeCoverageIgnore
     */
    public function profileBoxAction()
    {
        return new ViewModel(array(
            'params' => $this->params()->fromRoute('params')
        ));
    }

    public function signinAction()
    {
        $signInForm = new SigninForm();

        $request = $this->getRequest();

        if (($request instanceof Request) && $request->isPost()) {
            $signInForm->setData($request->getPost());

            if ($signInForm->isValid()) {
                $data = $signInForm->getData();

                /** @var $equalsAdapter \ZFS\User\Adapter\EqualsAdapter */
                $equalsAdapter = $this->getServiceLocator()->get('ZFS\EqualsAdapter');
                $equalsAdapter->setCredentials($data['email'], $data['password']);

                /** @var $authService \Zend\Authentication\AuthenticationService */
                $authService = $this->getServiceLocator()->get('ZFS\AuthService');
                $authResult  = $authService->setAdapter($equalsAdapter)->authenticate();

                if ($authService->hasIdentity()) {
                    if ($data['rememberMe']) {
                        $this->AuthPlugin()->generateCookie();
                    }

                    return $this->redirect()->toRoute($this->routes->login_redirect);
                }

                switch ($authResult->getCode())
                {
                    case Result::FAILURE_IDENTITY_NOT_FOUND:
                        $signInForm->get('email')->setMessages($authResult->getMessages());
                        break;
                    case Result::FAILURE_CREDENTIAL_INVALID:
                        $signInForm->get('password')->setMessages($authResult->getMessages());
                        break;
                    case Result::FAILURE:
                    default:
                        foreach ($authResult->getMessages() as $message) {
                            $this->flashMessenger()->addWarningMessage($message);
                        }
                        break;
                }
            }
        }

        return new ViewModel(array(
            'form' => $signInForm,
        ));
    }

    public function signoutAction()
    {
        $this->getServiceLocator()->get('ZFS\AuthService')->clearIdentity();

        $this->AuthPlugin()->removeCookie();

        return $this->redirect()->toRoute($this->routes->redirect);
    }

    public function signupAction()
    {
        $request = $this->getRequest();

        $signUpForm = new SignupForm();

        if (($request instanceof Request) && $request->isPost()) {
            $signUpForm->setData($request->getPost());

            if ($signUpForm->isValid()) {
                $data = $signUpForm->getData();

                /** @var $userExist UserObject */
                $userExist = $this->getUserGateway()->selectOne((array('email' => $data['email'])));

                if (!$userExist) {
                    $user = new UserObject();
                    $user->email = $data['email'];
                    $user->name  = $data['username'];
                    $user->status = AuthObject::STATUS_ACTIVE;

                    $this->getUserGateway()->saveObject($user);

                    $this->getUserGateway()->setUserRoles($user->id, array(RoleObject::ROLE_GUEST));

                    $auth_id = $this->AuthPlugin()->generateEquals($user, $data['password']);

                    if ($this->MailPlugin()->sendActivation($auth_id, AuthActivation::ACTION_ACTIVATION)) {
                        $this->flashMessenger()->addSuccessMessage('signup_success', array('email' => $user->email));
                    } else {
                        $this->flashMessenger()->addErrorMessage('mail_error');
                    }

                    return $this->redirect()->toRoute($this->routes->redirect);

                } else {
                    /** @var $authRow AuthObject */
                    $authRow = $this->getAuthGateway()->selectOne(array(
                        'foreign_key' => $data['email'],
                        'provider'    => AuthObject::PROVIDER_EQUALS
                    ));

                    if (!$authRow) {
                        $auth_id = $this->AuthPlugin()->generateEquals($userExist, $data['password']);

                        if ($this->MailPlugin()->sendActivation($auth_id, AuthActivation::ACTION_ACTIVATION)) {
                            $this->flashMessenger()->addInfoMessage(
                                'signup_mail_exist',
                                array('email' => $data['email'])
                            );

                        } else {
                            $this->flashMessenger()->addErrorMessage('mail_error');
                        }

                        return $this->redirect()->toRoute($this->routes->redirect);
                    }

                    switch ($authRow->status)
                    {
                        case AuthObject::STATUS_PENDING:
                            $this->flashMessenger()->addInfoMessage('pending', array('email' => $data['email']));
                            break;

                        case AuthObject::STATUS_ACTIVE:
                            $signUpForm->get('email')->setMessages(array('exist' => array('Email is already taken.')));
                            break;
                    }
                }
            }
        }

        return new ViewModel(array(
            'form' => $signUpForm
        ));
    }
}
