<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.03.15
 * Time: 12:06
 */

namespace ZFS\User\Controller;

use ZFS\User\Form\Base as BaseForm;
use ZFS\User\Form\Reset as ResetForm;
use ZFS\User\Model\Object\AuthActivation;
use ZFS\User\Model\Object\Auth as AuthObject;
use ZFS\User\Model\Object\User as UserObject;
use ZFS\User\Model\Object\Role as RoleObject;
use Zend\View\Model\ViewModel;
use Zend\Http\Request;

class ActivationController extends AbstractActionController
{
    public function recoveryAction()
    {
        $request = $this->getRequest();

        $code = null;
        if ($request instanceof Request) {
            $code = $request->getQuery('code');
        }

        if ($code) {
            /** @var $authActivationGateway \ZFS\Common\Model\Gateway\BaseGateway */
            $authActivationGateway = $this->getServiceLocator()->get('AuthActivationGateway');

            /** @var $authActivation AuthActivation */
            $authActivation = $authActivationGateway->selectOne(array('code' => $code));

            if (!$authActivation) {
                return $this->redirect()->toRoute($this->routes->redirect);
            }

            $resetForm = new ResetForm();

            if (($request instanceof Request) && $request->isPost()) {
                $resetForm->setData($request->getPost());

                if ($resetForm->isValid()) {
                    $data = $resetForm->getData();

                    /** @var $auth AuthObject */
                    $auth = $this->getAuthGateway()->selectOne(array('id' => $authActivation->authId));

                    /** @var $userGateway \ZFS\User\Model\Gateway\UserGateway */
                    $userGateway = $this->getServiceLocator()->get('UserGateway');

                    /** @var $user UserObject */
                    $user = $userGateway->selectOne(array('id' => $auth->userId));

                    $this->AuthPlugin()->generateEquals($user, $data['password']);

                    $authActivationGateway->deleteObject($authActivation);

                    $this->flashMessenger()->addSuccessMessage('password_changed', array('email' => $user->email));

                    return $this->redirect()->toRoute($this->routes->redirect);
                }
            }

            return new ViewModel(array(
                'form' => $resetForm
            ));
        }

        $recoveryForm = new BaseForm('recovery');

        if (($request instanceof Request) && $request->isPost()) {
            $recoveryForm->setData($request->getPost());

            if ($recoveryForm->isValid()) {
                $data = $recoveryForm->getData();

                /** @var $auth AuthObject */
                $auth = $this->getAuthGateway()->selectOne(array(
                    'foreign_key' => $data['email'],
                    'provider'    => AuthObject::PROVIDER_EQUALS
                ));

                if (!$auth) {
                    $this->flashMessenger()->addWarningMessage('not_found', array('email' => $data['email']));
                } else {
                    switch ($auth->status)
                    {
                        case AuthObject::STATUS_PENDING:
                            $this->flashMessenger()->addWarningMessage('pending', array('email' => $data['email']));
                            break;

                        case AuthObject::STATUS_DELETED:
                            $this->flashMessenger()->addWarningMessage('deleted', array('email' => $data['email']));
                            break;

                        case AuthObject::STATUS_DISABLED:
                            $this->flashMessenger()->addWarningMessage('inactive', array('email' => $data['email']));
                            break;

                        case AuthObject::STATUS_ACTIVE:
                            if ($this->MailPlugin()->sendActivation($auth->id, AuthActivation::ACTION_RECOVERY)) {
                                $this->flashMessenger()->addSuccessMessage(
                                    'recovery',
                                    array('email' => $data['email'])
                                );
                            } else {
                                $this->flashMessenger()->addErrorMessage('mail_error');
                            }
                            break;
                    }
                }

                return $this->redirect()->toRoute($this->routes->redirect);
            }
        }

        return new ViewModel(array(
            'form'    => $recoveryForm
        ));
    }

    public function activationAction()
    {
        $request = $this->getRequest();

        if (!$request instanceof Request) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        $requestData = $request->getQuery();

        /** @var $authActivation AuthActivation */
        $authActivation = $this->getAuthActivationGateway()->selectOne(array('code' => $requestData['code']));

        if (!$authActivation) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        /** @var $auth AuthObject */
        $auth = $this->getAuthGateway()->selectOne(array('id' => $authActivation->authId));

        if (!$auth) {
            return $this->redirect()->toRoute($this->routes->redirect);
        }

        $datetime1 = new \DateTime(); // now
        $datetime2 = new \DateTime($authActivation->expired);
        $interval  = $datetime1->diff($datetime2);

        // if activation code expired
        if ($interval->invert) {
            $this->flashMessenger()->addErrorMessage('activation_expired');
            $this->getAuthActivationGateway()->deleteObject($authActivation);
        } else {
            switch ($requestData['action'])
            {
                case AuthActivation::ACTION_CANCEL:
                    $this->getAuthActivationGateway()->deleteObject($authActivation);
                    break;

                case AuthActivation::ACTION_ACTIVATION:
                case AuthActivation::ACTION_CONFIRM:
                    $auth->status = AuthObject::STATUS_ACTIVE;
                    $this->getAuthGateway()->updateObject($auth);
                    $this->getUserGateway()->setUserRoles($auth->userId, array(RoleObject::ROLE_USER));
                    $this->flashMessenger()->addSuccessMessage('activation_success');
                    break;

                case AuthActivation::ACTION_RECOVERY:
                    return $this->redirect()->toRoute(
                        'zfsuser/recovery',
                        array(),
                        array('query' => array(
                            'code' => $requestData['code']
                        ))
                    );
                    break;

                case AuthActivation::ACTION_OLD_EMAIL_CONFIRM:
                    $data = unserialize($authActivation->data);

                    $this->getAuthActivationGateway()->deleteObject($authActivation);

                    $mailResult = $this->MailPlugin()->sendActivation(
                        $authActivation->authId,
                        AuthActivation::ACTION_NEW_EMAIL_CONFIRM,
                        array('email' => $data['email']),
                        $data['email']
                    );

                    if ($mailResult) {
                        $this->flashMessenger()->addSuccessMessage(
                            'new_email_confirm',
                            array('email' => $data['email'])
                        );
                    } else {
                        $this->flashMessenger()->addErrorMessage('mail_error');
                    }


                    break;

                case AuthActivation::ACTION_NEW_EMAIL_CONFIRM:
                    $data = unserialize($authActivation->data);

                    $auth = $this->getAuthGateway()->selectOne(array('id' => $authActivation->authId));

                    $auth->foreignKey = $data['email'];

                    $this->getAuthGateway()->updateObject($auth);
                    $this->getUserGateway()->update(array('email' => $data['email']), array('id' => $auth->userId));

                    $this->flashMessenger()->addSuccessMessage('email_changed');

                    $this->getAuthActivationGateway()->deleteObject($authActivation);
                    $this->AuthPlugin()->updateIdentity();
                    break;
            }
        }

        return $this->redirect()->toRoute($this->routes->redirect);
    }
}
