<?php
/**
 * User: dev
 * Date: 23.02.15 11:36
 */

namespace ZFS\User\Controller;

use Zend\Db\Sql\Predicate\In;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use ZFS\Common\Controller\AbstractManagementController;
use Zend\View\Model\ViewModel;
use ZFS\User\Form\Management\User as UserForm;
use ZFS\User\Model\Object\Auth as AuthObject;
use ZFS\User\Model\Object\User as UserObject;
use Zend\Http\Request;
use ZFS\User\Controller\Plugin\AuthPlugin;
use ZFS\User\Model\Gateway\Traits\GatewayTrait;

/**
 * @method AuthPlugin AuthPlugin()
 */
class UserManagementController extends AbstractManagementController
{
    use GatewayTrait;

    /**
     * @permission user_management
     * @see onDispatch()
     */
    public function indexAction()
    {
        $order          = $this->params()->fromQuery('order', null);
        $orderDirection = $this->params()->fromQuery('orderDirection', null);
        $filterColumn   = $this->params()->fromQuery('filterColumn', null);
        $filter         = $this->params()->fromQuery('filter', null);

        /** @var $usersTS \ZFS\User\Model\TransactionScript\Users */
        $usersTS = $this->getServiceLocator()->get('UsersTransactionScript');
        $select = $usersTS->select($order, $orderDirection, $filterColumn, $filter);

        $paginator = new Paginator(new DbSelect(
            $select,
            $this->getUserGateway()->getAdapter()
        ));

        $page = (int)$this->params()->fromQuery('page');

        $itemsPerPage = 10;

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($itemsPerPage);

        $viewModel = new ViewModel(array(
            'paginator'    => $paginator,
            'itemsPerPage' => $itemsPerPage,
            'query'        => $this->params()->fromQuery()
        ));

        $request = $this->getRequest();
        if (($request instanceof Request) && $request->isXmlHttpRequest()) {
            $viewModel->setTemplate('zfs/user-management/table.phtml');
            $viewModel->setTerminal(true);
        }

        $this->getEventManager()->trigger('ZFS\Dashboard\Event\Navigation\Sidebar');

        return $viewModel;
    }

    /**
     * @permission user_management
     * @see onDispatch()
     */
    public function createAction()
    {
        $userId = (int)$this->params()->fromRoute('id');

        $userForm = new UserForm();

        $userForm->setRoles($this->getRoleGateway()->select());

        $request = $this->getRequest();
        if (($request instanceof Request) && $request->isPost()) {
            $userForm->setData($request->getPost());

            if ($userForm->isValid()) {
                $data = $userForm->getData();

                $userExist = $this->getUserGateway()->selectOne(array('email' => $data['email']));

                if (!$userExist) {
                    $user = new UserObject();
                    $user->email  = $data['email'];
                    $user->name   = $data['name'];
                    $user->status = $data['status'];

                    if ($data['id']) {
                        $user->id = $data['id'];
                        $user->updated = date('Y-m-d H:i:s', strtotime('now'));
                        $this->getUserGateway()->updateObject($user);
                    } else {
                        $this->getUserGateway()->insertObject($user);
                    }

                    $this->getUserGateway()->setUserRoles($user->id, $data['roles']);

                    if ($data['password']) {
                        $this->AuthPlugin()->generateEquals($user, $data['password'], AuthObject::STATUS_ACTIVE);
                    }

                    return $this->redirect()->toRoute('zfsusermanagement');
                } else {
                    $this->flashMessenger()->addWarningMessage('email_exist', array('email' => $data['email']));
                }
            } else {
                foreach ($userForm->getInputFilter()->getMessages() as $messages) {
                    foreach ($messages as $message) {
                        if (!is_array($message)) {
                            $this->flashMessenger()->addWarningMessage($message);
                        }
                    }
                }
            }
        }

        if ($userId) {
            $user = $this->getUserGateway()->selectOne(array('id' => $userId));

            if ($user) {
                $user = $user->toArray();

                $userRoles = $this->getUserGateway()->getUserRoles($userId);
                $roles = array();

                foreach ($userRoles as $role) {
                    $roles[$role['role_name']] = $role['role_name'];
                }

                $user['roles'] = $roles;

                $userForm->setData($user);
            }
        }

        $this->getEventManager()->trigger('ZFS\Dashboard\Event\Navigation\Sidebar');

        return new ViewModel(array(
            'form' => $userForm,
        ));
    }

    /**
     * @permission user_management
     * @see onDispatch()
     */
    public function deleteAction()
    {
        $userId = (int)$this->params()->fromRoute('id');

        if ($userId) {
            $this->getUserGateway()->delete(array('id' => $userId));
        }

        $request = $this->getRequest();
        if (($request instanceof Request) && $request->isPost()) {
            $ids = $request->getPost('ids');
            $this->getUserGateway()->delete(new In('id', explode(',', $ids)));
        }

        return $this->redirect()->toRoute('zfsusermanagement');
    }

    /**
     * @codeCoverageIgnore
     */
    public function navigationAction()
    {
    }
}
