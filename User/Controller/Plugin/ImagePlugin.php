<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.03.15
 * Time: 16:29
 */

namespace ZFS\User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use ZFS\User\Controller\AbstractActionController;

class ImagePlugin extends AbstractPlugin
{
    /**
     * @param string $image
     * @param int    $userId
     * @param bool   $rewrite
     *
     * @return bool
     */
    public function saveAvatar($image, $userId, $rewrite = false)
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $imageService \ZFS\User\Services\ImageService */
        $imageService = $controller->getServiceLocator()->get('ZFS\Service\Image');
        return $imageService->saveAvatar($image, $userId, $rewrite);
    }
}
