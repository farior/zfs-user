<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.02.15
 * Time: 10:52
 */

namespace ZFS\User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use ZFS\User\Controller\AbstractActionController;

/**
 * @codeCoverageIgnore
 */
class MailPlugin extends AbstractPlugin
{
    /**
     * @param string $to
     * @param string $template
     * @param array  $params
     * @param string $name
     *
     * @return bool
     */
    public function sendMail($to, $template, $params = array(), $name = 'Name')
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $mailService \ZFS\User\Services\MailService */
        $mailService = $controller->getServiceLocator()->get('ZFS\Service\Mail');
        return $mailService->sendMail($to, $template, $params, $name);
    }

    /**
     * @param int    $auth_id
     * @param string $action
     * @param string $data
     * @param string $toEmail
     * @param int    $expired
     * @return bool
     */
    public function sendActivation($auth_id, $action, $data = null, $toEmail = null, $expired = 5)
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $mailService \ZFS\User\Services\MailService */
        $mailService = $controller->getServiceLocator()->get('ZFS\Service\Mail');
        return $mailService->sendActivation($auth_id, $action, $data, $toEmail, $expired);
    }
}
