<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 26.01.15
 * Time: 11:55
 */

namespace ZFS\User\Controller\Plugin;

use Zend\Authentication\Result;
use Zend\Http\Response;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Crypt\Password\Bcrypt;
use ZFS\User\Controller\AbstractActionController;
use ZFS\User\Model\Object\Auth;
use ZFS\User\Model\Object\AuthActivation;
use ZFS\User\Model\Object\User as UserObject;
use ZFS\User\Model\Object\Auth as AuthObject;
use ZFS\User\Model\Object\Role as RoleObject;
use Zend\Http\Header\SetCookie;
use ZFS\User\Model\Object\Identity;

class AuthPlugin extends AbstractPlugin
{
    /**
     * Time that the token remains valid
     *
     * remember me for 31 day 3600 * 24 * 31
     */
    const COOKIE_EXPIRATION_TIME = 2678400;

    /**
     * Generate cookie for current identity
     *
     * @return bool
     */
    public function generateCookie()
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->generateCookie();
    }

    /**
     * @param UserObject $user
     * @param string $password
     * @param string $status
     *
     * @return int
     */
    public function generateEquals($user, $password, $status = AuthObject::STATUS_PENDING)
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->generateEquals($user, $password, $status);
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     * @return AuthObject
     */
    public function generateSocial($socialData)
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->generateSocial($socialData);
    }

    /**
     * @return Identity
     */
    public function updateIdentity()
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->updateIdentity();
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     * @return bool
     */
    public function linkSocial($socialData)
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->linkSocial($socialData);
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     * @return bool|Result
     */
    public function authenticateSocial($socialData)
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->authenticateSocial($socialData);
    }

    /**
     * @param \ZFS\User\Social\Container\OpauthSocialContainer $socialData
     * @return Identity
     *
     * @codeCoverageIgnore
     */
    public function checkSocial($socialData)
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->checkSocial($socialData);
    }

    /**
     * @codeCoverageIgnore
     *
     * @return bool
     */
    public function removeCookie()
    {
        $controller = $this->getController();

        if (!$controller instanceof AbstractActionController) {
            return false;
        }

        /** @var $authService \ZFS\User\Services\AuthService */
        $authService = $controller->getServiceLocator()->get('ZFS\Service\Auth');
        return $authService->removeCookie();
    }
}
